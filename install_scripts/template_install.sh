#!/bin/bash


#  _____                 _   _
# |  ___|   _ _ __   ___| |_(_) ___  _ __  ___
# | |_ | | | | '_ \ / __| __| |/ _ \| '_ \/ __|
# |  _|| |_| | | | | (__| |_| | (_) | | | \__ \
# |_|   \__,_|_| |_|\___|\__|_|\___/|_| |_|___/
#
info() { printf "\e[1;34m$1\e[0m\n"; }


#  ____                                _     _ _
# |  _ \ _ __ ___ _ __ ___  __ _ _   _(_)___(_) |_ ___  ___
# | |_) | '__/ _ \ '__/ _ \/ _` | | | | / __| | __/ _ \/ __|
# |  __/| | |  __/ | |  __/ (_| | |_| | \__ \ | ||  __/\__ \
# |_|   |_|  \___|_|  \___|\__, |\__,_|_|___/_|\__\___||___/
#                             |_|
%%installs

#  ____       _
# / ___|  ___| |_ _   _ _ __
# \___ \ / _ \ __| | | | '_ \
#  ___) |  __/ |_| |_| | |_) |
# |____/ \___|\__|\__,_| .__/
#                      |_|

if [ -f /etc/rc.local ]; then
    info "Found an already existing rc.local file"
    sudo cp /etc/rc.local /etc/rc.local.bak
    sudo rm /etc/rc.local
    echo "Created a backup copy in /etc/rc.local.bak"
fi
sudo touch /etc/rc.local
sudo chmod 755 /etc/rc.local
echo "#!/bin/bash" | sudo tee -a /etc/rc.local > /dev/null

%%telegrambotstart

%%configuration

%%dotenv

%%doorstart
