#!/bin/bash


#  _____                 _   _
# |  ___|   _ _ __   ___| |_(_) ___  _ __  ___
# | |_ | | | | '_ \ / __| __| |/ _ \| '_ \/ __|
# |  _|| |_| | | | | (__| |_| | (_) | | | \__ \
# |_|   \__,_|_| |_|\___|\__|_|\___/|_| |_|___/
#
info() { printf "\e[1;34m$1\e[0m\n"; }


#  ____                                _     _ _
# |  _ \ _ __ ___ _ __ ___  __ _ _   _(_)___(_) |_ ___  ___
# | |_) | '__/ _ \ '__/ _ \/ _` | | | | / __| | __/ _ \/ __|
# |  __/| | |  __/ | |  __/ (_| | |_| | \__ \ | ||  __/\__ \
# |_|   |_|  \___|_|  \___|\__, |\__,_|_|___/_|\__\___||___/
#                             |_|


#  ____       _
# / ___|  ___| |_ _   _ _ __
# \___ \ / _ \ __| | | | '_ \
#  ___) |  __/ |_| |_| | |_) |
# |____/ \___|\__|\__,_| .__/
#                      |_|

if [ -f /etc/rc.local ]; then
    info "Found an already existing rc.local file"
    sudo cp /etc/rc.local /etc/rc.local.bak
    sudo rm /etc/rc.local
    echo "Created a backup copy in /etc/rc.local.bak"
fi
sudo touch /etc/rc.local
sudo chmod 755 /etc/rc.local
echo "#!/bin/bash" | sudo tee -a /etc/rc.local > /dev/null

info "Enabling telegram bot at startup..."
echo "python3 /home/matteo/Documents/IoT-project/main_telebot.py &" | sudo tee -a /etc/rc.local > /dev/null


info "Client configuration..."
mkdir -p /home/matteo/remote-smart-lab-doors


info "Configuring .env variables"
echo "DATABASE=/home/matteo/remote-smart-lab-doors/database.db" >> /home/matteo/Documents/IoT-project/.env
echo "DATABASE=/home/matteo/remote-smart-lab-doors/database.db" >> /home/matteo/Documents/IoT-project/telegrambot/.env
echo "SERVER_IP=192.168.1.1" >> /home/matteo/Documents/IoT-project/.env
echo "SERVER_IP=192.168.1.1" >> /home/matteo/Documents/IoT-project/telegrambot/.env
echo "ISSERVER=false" >> /home/matteo/Documents/IoT-project/.env
echo "ISSERVER=false" >> /home/matteo/Documents/IoT-project/telegrambot/.env
echo "LAB_ID=2" >> /home/matteo/Documents/IoT-project/.env
echo "LAB_ID=2" >> /home/matteo/Documents/IoT-project/telegrambot/.env


info "Enabling door script at startup..."
echo "python3 /home/matteo/Documents/IoT-project/main_door.py &" | sudo tee -a /etc/rc.local > /dev/null

