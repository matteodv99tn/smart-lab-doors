info "Installing opencv from source"
# info " --- UPDATING AND UPGRADING --- "
# sudo apt update
# sudo apt upgrade

info " --- INSTALLING DEPENDENCIES --- "
sudo apt install -y \
	cmake build-essential pkg-config git \
    libjpeg-dev libtiff-dev libjasper-dev libpng-dev libwebp-dev libopenexr-dev \
    libavcodec-dev libavformat-dev libswscale-dev libv4l-dev libxvidcore-dev libx264-dev libdc1394-22-dev libgstreamer-plugins-base1.0-dev libgstreamer1.0-dev \
    libgtk-3-dev libqt5gui5 libqt5webkit5 libqt5test5 python3-pyqt5 \
    libatlas-base-dev liblapacke-dev gfortran \
    libhdf5-dev libhdf5-103 \
    python3-dev python3-pip python3-numpy

pushd ~
info " --- CLONING OPENCV REPOS --- "
git clone https://github.com/opencv/opencv.git
pushd ~/opencv
git checkout 4.7.0
popd
git clone https://github.com/opencv/opencv_contrib.git
pushd ~/opencv_contrib
git checkout 4.7.0
popd

mkdir -p ~/opencv/build
cd ~/opencv/build
info " --- RUNNIN CMAKE --- "
cmake -D CMAKE_BUILD_TYPE=RELEASE \
    -D CMAKE_INSTALL_PREFIX=/usr/local \
    -D OPENCV_EXTRA_MODULES_PATH=~/opencv_contrib/modules \
    -D ENABLE_NEON=ON \
    -D ENABLE_VFPV3=ON \
    -D BUILD_TESTS=OFF \
    -D INSTALL_PYTHON_EXAMPLES=OFF \
    -D CMAKE_XCC_FLAGS=-latomic \
    -D OPENCV_PYTHON_INSTALL_PATH=lib/python3.9/dist-packages \
    -D BUILD_EXAMPLES=OFF ..

info " --- RUNNING MAKE --- "
make -j4

info " --- RUNNING MAKE INSTALL --- "
sudo make install

info " --- RUNNING LD CONFIG --- "
sudo ldconfig

info " --- REMOVING SOURCE FOLDER --- "
rm -rf ~/opencv
rm -rf ~/opencv_contrib
