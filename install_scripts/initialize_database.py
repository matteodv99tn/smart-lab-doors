import sys

from database import Database, User

db_name = sys.argv[1]
db = Database(db_name)
db.ensure_initialized()

user = User(name="Admin", surname="Admin")
user.insert_into_database(db)
db.commit()
