import os

from string import Template

ROOT_DIR = os.path.abspath(os.path.dirname(__file__) + "/..")
SCRIPTS_DIR = os.path.join(ROOT_DIR, "install_scripts")
TEMPLATE_SCRIPT = os.path.join(SCRIPTS_DIR, "template_install.sh")
INSTALL_SCRIPT = os.path.join(SCRIPTS_DIR, "install.sh")

template_args = {}
env_vars = []


class MyTemplate(Template):
    delimiter = '%%'


def main():
    ask_requirement_install()
    ask_telegram_bot()
    is_server = ask_server()
    if is_server:
        configure_server()
    else:
        configure_client()

    configure_door_id()

    enable_door_script()
    configure_dotenv()

    with open(TEMPLATE_SCRIPT, "r") as file_in:
        src = MyTemplate(file_in.read())
        res = src.substitute(template_args)
        print(res)

        with open(INSTALL_SCRIPT, "w") as file_out:
            file_out.write(res)


def to_rclocal(text: str):
    return f"echo \"{text}\" | sudo tee -a /etc/rc.local > /dev/null\n"


def ask_requirement_install():
    template_args["installs"] = ""

    text = "Install dependencies? [Y/n] "
    if input(text).lower() in ["y", "yes", ""]:
        template_args["installs"] = set_requirements()


def set_requirements():
    text = "info \"Installing dependencies...\"\n"
    with open(SCRIPTS_DIR+"/install_requirements.sh", "r") as file_in:
        text += file_in.read()

    if os.uname()[4][:3] == "arm":
        text += "info \"Installing OpenCV from source...\"\n"
        with open(SCRIPTS_DIR+"/install_opencv_source.sh", "r") as file_in:
            text += file_in.read()
    else:
        text += "info \"Installing OpenCV from pip...\"\n"
        text += "pip3 install opencv-python\n"

    return text


def ask_telegram_bot():
    template_args["telegrambotstart"] = ""

    text = "Enable telegram bot at startup? (Note: only one istance of the " \
           "bot should be running) [Y/n] "
    if input(text).lower() in ["y", "yes"]:
        cmd = "python3 " + os.path.join(ROOT_DIR, "main_telebot.py") + " &"
        info_msg = "info \"Enabling telegram bot at startup...\"\n"
        template_args["telegrambotstart"] = info_msg + to_rclocal(cmd)


def ask_server():
    text = "Will this device be server for the infrastructure? [Y/n] "
    if input(text).lower() in ["y", "yes", ""]:
        return True
    else:
        return False


def configure_server():
    template_args["configuration"] = "info \"Server configuration...\"\n"

    cmd = "export PYTHONPATH=" + ROOT_DIR + ":$PYTHONPATH\n"
    template_args["configuration"] += cmd

    cmd = "python3 " + SCRIPTS_DIR + "/initialize_database.py " \
          + ROOT_DIR + "/database.db\n"
    template_args["configuration"] += cmd
    template_args["configuration"] += "echo \"Server initialized\"\n"

    cmd = "sudo ln -s " + ROOT_DIR + " /sld\n"
    template_args["configuration"] += cmd

    env_vars.append(("DATABASE", ROOT_DIR + "/database.db"))
    env_vars.append(("ISSERVER", "true"))


def configure_client():
    HOME_DIR = os.getenv("HOME")
    REMOTE_DIR = HOME_DIR + "/remote-smart-lab-doors"
    REMOTE_DB = REMOTE_DIR + "/database.db"
    template_args["configuration"] = "info \"Client configuration...\"\n"
    template_args["configuration"] += f"mkdir -p {REMOTE_DIR}\n"

    text = "Enter the IP address of the server to connect to: "
    ip = input(text)

    env_vars.append(("DATABASE", REMOTE_DB))
    env_vars.append(("SERVER_IP", ip))
    env_vars.append(("ISSERVER", "false"))


def configure_door_id():
    text = "Enter the ID of the lab. with which permissions are checked: "
    door_id = input(text)
    env_vars.append(("LAB_ID", door_id))


def configure_dotenv():
    template_args["dotenv"] = "info \"Configuring .env variables\"\n"
    files = [ROOT_DIR + "/.env",
             ROOT_DIR + "/telegrambot/.env"]
    for var, value in env_vars:
        for f in files:
            template_args["dotenv"] += f"echo \"{var}={value}\" >> {f}\n"


def enable_door_script():
    cmd = "python3 " + os.path.join(ROOT_DIR, "main_door.py") + " &"
    info_msg = "info \"Enabling door script at startup...\"\n"
    template_args["doorstart"] = info_msg + to_rclocal(cmd)


if __name__ == "__main__":
    main()
