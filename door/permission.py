from __future__ import annotations

def check_permission(self, user) -> int | None:
    """ Checks that the user has an active permission for the lab.

    On success it returns the permission id, on fail returns none
    """
    query = """
            SELECT permission_id
            FROM permissions
            WHERE user_id = ?
                AND lab_id = ?
            """
    params = (user.id, self.id)
    permission_id = self.db.execute(query, params).fetchone()

    if permission_id is None:
        return None
    else:
        return permission_id[0]
