from __future__ import annotations

from databases import DatabaseInterface, get_lab_name_from_id


class Door():
    """ Door class """

    id:         int = None
    name:       str = None
    db:         DatabaseInterface = None
    showimage:  bool = True

    def __init__(self, database: DatabaseInterface, lab_id: int):
        """ Door constructor """
        self.db = database
        self.id = lab_id
        self.name = get_lab_name_from_id(self.db, lab_id)

    from .permission import check_permission
    from .qrcode import \
        qrcode_poll,\
        qrcode_process_data,\
        qrcode_process_studentecard,\
        qrcode_process_guestdata

    def main(self):
        qr_data = self.qrcode_poll()
        user_data = self.qrcode_process_data(qr_data)
        if user_data is None:
            return False

        user, image = user_data

        perm_id = self.check_permission(user)
        if perm_id is None:
            print("No permission for the lab")
            return False

        if user.is_student:
            # Perform the check on the image
            pass

        print("Valid permissions")

        query = "INSERT INTO accesses (permission_id) VALUES (?)"
        params = (perm_id,)
        self.db.execute(query, params)
        self.db.commit()

        return True

    def __str__(self):
        return f"Door for the laboratory {self.name} (ID {self.id})"
