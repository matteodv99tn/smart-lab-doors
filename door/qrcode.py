from __future__ import annotations
""" All the following functions should be considered as member of the Door
class.
"""
import cv2
import urllib.request
import base64

from PIL import Image
from bs4 import BeautifulSoup
from io import BytesIO

from databases import User, get_user_by_fullname
from utils import check_qr_validity
from utils.data_encryption import decrypt_data


qr_detector = cv2.QRCodeDetector()
video_capture = cv2.VideoCapture(0)


def qrcode_poll(self) -> str:
    """ Polls for a new QR code in the camera and returns the read string """
    image_detected = False
    while not image_detected:
        _, img = video_capture.read()
        if self.showimage:
            cv2.imshow("QR Code Scanner", img)
        if cv2.waitKey(1) == ord('q'):
            break

        qrdata, points, straight_qrcode = qr_detector.detectAndDecode(img)
        if qrdata != "":
            image_detected = True
            cv2.destroyAllWindows()
            return qrdata


def qrcode_process_data(self, data: str) -> tuple[User, Image] | None:
    if data.startswith("https://webapps.unitn.it/studentecard"):
        return self.qrcode_process_studentecard(data)
    else:
        decrypted_data = decrypt_data(bytes(data, "utf-8")).split(":")
        if decrypted_data[0] != "guestuseraccess":
            print("Invalid QR code given")
            return None

        qr_id = int(decrypted_data[1])
        return self.qrcode_process_guestdata(qr_id)


def qrcode_process_studentecard(self, data: str) -> tuple[User, Image] | None:
    """ Processes the data from a studentecard QR code """
    page_content = urllib.request.urlopen(data).read().decode("utf-8")
    soup = BeautifulSoup(page_content, "html.parser")

    if page_content.find("alert alert-danger") != -1:
        print("Expired QR code")
        return None

    # Extract user object
    full_name = soup.find(name="h3",
                          attrs={"class": "text-dark"}).text
    print("Door: student ecard full name:", full_name)
    user = get_user_by_fullname(self.db, full_name)
    if user is None:
        print("User not found")
        return None

    # Extract image
    image_tags = soup.find_all(name="img")
    image = None
    for img in image_tags:
        if img["src"].find("data:image") != -1:
            imgdata = bytes(img["src"].split(",")[1], "utf-8")
            image = Image.open(BytesIO(base64.b64decode(imgdata)))
            break
    return user, image


def qrcode_process_guestdata(self, qr_id: int) -> User:
    out = check_qr_validity(self.db, qr_id)
    if out == False:
        print("QR code expired or used mored than once")
        return None
    else:
        return out, None
