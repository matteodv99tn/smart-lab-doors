from __future__ import annotations
from .door import Door
from RPi import GPIO


class PiDoor(Door):
    """ PiDoor class """
    GREEN_LED = 24
    RED_LED = 23

    def __init__(self, database: DatabaseInterface, lab_id: int):
        """ PiDoor constructor """
        super().__init__(database, lab_id)
        self.showimage = False
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.GREEN_LED, GPIO.OUT)
        GPIO.setup(self.RED_LED, GPIO.OUT)

    def led_green_on(self):
        """ Turn on green LED """
        GPIO.output(self.GREEN_LED, GPIO.HIGH)

    def led_green_off(self):
        """ Turn off green LED """
        GPIO.output(self.GREEN_LED, GPIO.LOW)

    def led_red_on(self):
        """ Turn on red LED """
        GPIO.output(self.RED_LED, GPIO.HIGH)

    def led_red_off(self):
        """ Turn off red LED """
        GPIO.output(self.RED_LED, GPIO.LOW)

    def main(self):
        self.led_green_on()
        self.led_red_on()

        give_access = super().main()

        if give_access:
            self.led_green_on()
            self.led_red_off()
        else:
            self.led_green_off()
            self.led_red_on()
