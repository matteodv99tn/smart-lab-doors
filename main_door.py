from door import Door
from telegrambot import db


def main():

    LAB_ID = 3

    try:
        from door.pidoor import PiDoor
        door = PiDoor(database=db, lab_id=LAB_ID)
        print("Using rasperry smart lab door configuration")
    except ImportError:
        door = Door(database=db, lab_id=LAB_ID)
        print("Using default smart lab door configuration")

    print(str(door))
    door.main()
    pass


if __name__ == "__main__":
    main()
