#!/bin/bash

rm docs/_build -rf
rm html -rf
# sphinx-apidoc -o docs/_build .
sphinx-build -b html docs html

firefox --new-window html/index.html
