Conclusions
===========

At the current state, the project is not ready for a everyday use.

It's a good proof-of-concept, and shows the potentiality of having an infrastructure that can help with the accesses to the laboratories of the univerisity, and with an interface that's simple.

Still there are some challenges that needs to be faced if the project wants to be used for a real application. In particular:

- data encryption: data that contains sensitive information is, at the current state, encrypted using a symmetric crypthography. This is the most intuitive approach, but comes with some caveats: key must be manually copied from the server to each edge node. Other communication protocol, such MQTT, can't be used to share the encryption key, since it invalidates the privacy concern.
  A possible solution could be to use asymmetric encryption where the private key can reside only on the server side, while the public key can be easily shared through MQTT.
- automation: the current system requires a manual boot of each script that makes using edge devices not easy. This procedure can be done with low effort, but at the stage no formal verification has been carried out.
  Furthermore the code isn't well suited to handle all possible exceptions that might be thrown, especially while dealing with the telegram bot and the database communication, that are now requiring a human oversight.
