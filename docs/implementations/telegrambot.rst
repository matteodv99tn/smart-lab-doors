Telegram Bot
============

The easiest way to interact with the database is by means of the `telegram bot <https://t.me/unitnlabaccessbot>`_.
By using proper commands (the ones starting with the ``/`` character), different operations can be performed; some are available to all users, while some are administrator specific.

Commands of general interest are

- ``/start``: starts the interaction with the bot. Based on the users telegram username, the bot is able to recognize if the person is already registered to the platform. If it's not the case, the registration procedure is performed. The bot is also able to identify if the ongoing registration matches information of an already registered user (whose telegram username however wasn't provided yet) and so just bounds the chat with the already given data;

    .. note::
       During the registration phase, the bot prompts whether the user is a student or a guest. In the first case the user is automatically added to server since it's required for them to have, later on, to have a valid student eCard to access the laboratories.
       Conversely, guests doesn't have such feature and to ensure safety, registration to the platform has to be approved by an administrator.

- ``/help``: the bot describes himself and briefly explains the available commands.

User-sided commands
-------------------
All users, upon registration, are able to call the following commands:

- ``/newpermission``: in this case a menu is displayed, showing all laboratories that the user might ask a permission for, i.e. the ones with a currently active permission are not shown. This is only a request, since it's up to the laboratory administrator to decide to accept it and with which timestamp;
- ``/mypermissions``: shows all currently active permission for the user, as well as their expiration date;
- ``/qrcode`` (for guests only): generates a QR code that can be used to access the laboratory. Such QR code has a timed validity (usually 5 minutes), after which the bot automatically deletes the image.


Admin-sided commands
--------------------
Administrators of one or more laboratories are able to call the following commands:

- ``/adduser``: let the administrator register a new user to the platform. To reduce usage friction, after registration the bot directly asks if permissions should be given to such user for specific laboratories;

  .. warning::
     At the current stage this procedure doesn't check against already present users, so it should be used with care.

- ``/managepermissions``: shows a menu with all currently active permissions for a laboratory and allows to manage them by either changing the note and postponing or ceasing permissions;
- ``/givepermission``: let the administrator give a permission to a user that's already registered to the database.


Experimental
------------
During this first preliminary tests, the bot has also the additional equivalent commands  ``/issue`` and ``/newissue``: whenever a user finds something strange and want to report to the developer, calling this commands allows to notify everyone that's interested by creating an issue inside the `gitlab's repository <https://git.iotn.it/matteodv99/smart-lab-doors/-/issues>`_.
