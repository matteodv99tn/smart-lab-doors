Introduction
============

The project call `Smart Lab Doors` aims at developing a user friendly platform for regulating the accesses to the laboratories of the university.

The work done consists in the following 3 components

- a telegram bot: main interaction with the service is provided by means of a telegram bot ``@unitnlabaccessbot`` that allows students to register to the platform and ask permissions for accessing the laboratories. With the same interface, professors or other administrators are allowed to grant accesses to user (or deny their request), as well as manage the laboratory status;
- a qr code checker: to access the laboratory, users must provide a valid QR code in order to access the lab. In this case, a Raspberry Pi 3B with the camera module has been employed to demonstrate the potentiality of the system;
- a visualization tool: in this case a `Grafana <https://grafana.com/>`_ dashboard has been developed to provide a good view of all collected informations in the database.


Data storage and communication protocols
----------------------------------------

Conversely to what we saw during the course, for the development of this project I preferred using relational SQL databases since the problem is well defined and data to be stored are homogeneous.

In the preliminary part of the development `Sqlite <https://www.sqlite.org/index.html>`_ has been used, since it doesn't require any setup and has no connection issues. However for the final solution, a server based solution has been used to have an easier and safer communication between multiple agents, since the final architecture is expected to have a central server with different edge devices at the different laboratories doors.

Among different free SQL relational databases, `Mariadb <https://mariadb.org/>`_ has been choosen since it's installed by default in the `Raspberry Pi OS <https://www.raspberrypi.com/software/operating-systems/>`_; still switching to `MySql <https://www.mysql.com/it/>`_ should be zero cost, since the query language is the same.

.. note::
   Generally switching SQL server is usually straightfarward, due to the standardize nature of the query language.
   However each distributor might have it's own peculiarities as experienced while migrating from Sqlite to Mariadb.
