Data visualization
==================

The last key element for this project is a `Grafana <https://grafana.com/>`_ dashboard that can be used by administrator to intuitively monitor the system behavior.

.. image:: ../images/dashboard.png
   :width: 1600

As here shown, the dashboard contains a set of different tables, divided mainly in 2 sections:

- the upper portion contains informations that are specific to a laboratory;
- the lower section contains information regarding the whole database.

Laboratory data
---------------

As above introduced, the upper section contains information specific to a given laboratory. When multiple laboratories are available, the first row contains a drop-down menu that allows to select which laboratory to visualize.

The upper-left pane displays all active permissions for the laboratory, ordered by closness to expiration.
Just beneath a smaller table provide a list of all administrators.

On the right side instead a table reports the list of all latest accesses.


Database data
-------------

The lower section instead shows the list of all the registered users to the service with the respective IDs that might be helpfull to administrators for assigning permission through the command-line interface of the database.

On the right side instead all administrators for all laboratories are shown.
