.. Smart Lab Doors documentation master file, created by
   sphinx-quickstart on Thu Aug  3 20:21:31 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Smart Lab Doors's documentation!
===========================================

.. toctree::
   :maxdepth: 1
   :caption: Project description

   implementations/introduction
   implementations/database
   implementations/telegrambot
   implementations/check
   implementations/grafana
   implementations/conclusions

.. toctree::
   :maxdepth: 1
   :caption: Python code

   code/database
   code/telegrambot

.. toctree::
   :maxdepth: 1
   :caption: Setups and tutorials

   tutorials/installation
   tutorials/mariadbconfiguration
   tutorials/sld_database
   tutorials/quickstart
   tutorials/dashboardinstall


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
