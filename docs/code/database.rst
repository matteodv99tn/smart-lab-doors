Databases package
=================

Description
-----------

.. automodule:: databases
   :undoc-members:


Interface
---------
.. autoclass:: databases.DatabaseInterface
   :members:
   :undoc-members:

Implementations
---------------
.. autoclass:: MariaDatabase

.. autoclass:: SqliteDatabase


Convenience classes
-------------------
.. autoclass:: User
   :members:
