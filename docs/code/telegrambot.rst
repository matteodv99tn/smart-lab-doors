Telegram bot package
====================

In this page, all modules and subpackages for the telegram bot are presented.
No documentation for each single function is provided, since their task are
usually trivial and self-explanatory based on the code context.

Package moduled
---------------

.. automodule:: telegrambot
   :undoc-members:

.. automodule:: telegrambot.utils
   :undoc-members:

.. automodule:: telegrambot.telegrambot_database
   :undoc-members:

.. automodule:: telegrambot.command_handler
   :undoc-members:

.. automodule:: telegrambot.gitlab_integration
   :undoc-members:


User subpackage
---------------

.. automodule:: telegrambot.user.mypermissions
   :undoc-members:

.. automodule:: telegrambot.user.newpermission
   :undoc-members:

.. automodule:: telegrambot.user.qrcode
   :undoc-members:

.. automodule:: telegrambot.user.registration
   :undoc-members:


Admin subpackage
----------------

.. automodule:: telegrambot.admin.givepermission
   :undoc-members:

.. automodule:: telegrambot.admin.managepermissions
   :undoc-members:

