Dashboard setup
===============

Installation
------------

As visualization tool, `Grafana <https://grafana.com/>`_ has been used. For it's installation, please refer to the `official installation documentation <https://grafana.com/grafana/download>`_.

Installation on raspberry have been used using the debian package as follows:

.. code-block:: shell

   sudo apt-get install -y adduser libfontconfig1
   wget https://dl.grafana.com/enterprise/release/grafana-enterprise_9.5.2_armhf.deb
   sudo dpkg -i grafana-enterprise_9.5.2_armhf.deb

Note that the version used is ``9.5.2``; installation of versions above ``10.0.0`` had issues with the raspberry.
Furthermore the exported json is not guaranteed to work on other versions.


Importing the dashboard
-----------------------

Source setup
^^^^^^^^^^^^

Before importing the proper dashboard json file, the datasource must be configured.

.. image:: ../images/datasource_p1.png
   :width: 250

In the `administrator` panel of grafana, under the `data sources` tab it's possible to add a new source. As shown, the source type is `MySQL`.

.. image:: ../images/datasource_p2.png
   :width: 400

The name of the source inside the grafana environment is not a critical, still choose a naming that's convenient.

The host should be modified by inserting the correct IP address of the machine hosting the MariaDB server (the port is by default 3306). The `database` entry must be as shown: `smartlabdoors`.

The user (and corresponding password) field must be filled with credentials of a user that at least has read write access to the database.
As suggested by Grafana, for security issue the user shouldn't have write access; for this reason the following statement create a new user `grafanavisualizer` with password `grafana` for such operation:

.. code-block:: sql

   MariaDB [(none)]> GRANT SELECT ON smartlabdoors.*
            TO 'grafanavisualizer'@'%'
            IDENTIFIED BY 'grafana';
   MariaDB [(none)]> FLUSH PRIVILEGES;


Dashboard import
^^^^^^^^^^^^^^^^

The dashboard template is already present in the repository as the file ``grafana_dashboard.json`` inside the ``config`` directory.

In the `dashboard` panel of grafana it's possible to create a new dashboard; while doing so select the `import` method with which the json file can be loaded.

.. image:: ../images/error_p1.png
   :width: 600

As shown in the image, when importing the dashboard all panes present an error. This is due to the missing connection with a properly configured database.

To fix this issue hover to the top-right corner of each pane and select `edit`.

.. image:: ../images/error_p2.png
   :width: 800

In the lower section where the query is defined, select the `data source` defined in the previous. You can `run query` to see that the data are now visualizing and you can save the modification.
