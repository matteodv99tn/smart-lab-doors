SmartLabDoors database
======================

In this page are briefly summarized passages to get going with a MariaDB server set-up for the project. If some of the commands are not clear, please refer to :ref:`MariaDB package installation and setup`.

Initialization
--------------

To facilitate the setup of the database, once the MariaDB service is running it's possible to run the bootstrap script that creates the database and setups the tables as well as automatic triggers.

.. code-block:: shell

   MariaDB [(none)]> source /path/to/SmartLabDoors/config/bootstrap.sql
   # or equivalently
   $ sudo mariadb < /path/to/SmartLabDoors/config/bootstrap.sql


For clarity, this is the source code of the script that runs:

.. include:: ../../config/bootstrap.sql
  :literal:


Database management
-------------------

Here are reported some commands that an administrator that can log directly into the database might be interested in.


.. warning::
   In general most of the following operations can be carried out more easily via the telegram bot.


User registration
^^^^^^^^^^^^^^^^^
To add a new user to the database

.. code-block:: sql

   MariaDB [(smartlabdoors)]> INSERT INTO users(name, surname, is_student)
        VALUES ("Mario", "Rossi", TRUE)
        RETURNING user_id;

.. note::
   Indentation in sql code is not necessary, but it's clearer to understand


.. note::
   The ``RETURNING`` statement is not necessary for registering a user,
   but it might be helpful for editing, as in the following example.


We can use the ``UPDATE`` statement to modify some fields, like

.. code-block:: sql

   MariaDB [(smartlabdoors)]> UPDATE users
        SET telegram="mariorossi"
        WHERE user_id = 21;

or

.. code-block:: sql

   MariaDB [(smartlabdoors)]> UPDATE users SET telegram="mariorossi"
        WHERE name LIKE "Mario" AND surname LIKE "Rossi";


Laboratories registration
^^^^^^^^^^^^^^^^^^^^^^^^^

Similarly to how users are added, laboratories can be created:

.. code-block:: sql

   MariaDB [(smartlabdoors)]> INSERT INTO laboratories(name)
        VALUES ("IoT lab")
        RETURNING lab_id;


We can then add administrators to the laboratory by firstly retrieving the ID of the user we want to promote

.. code-block:: sql

   MariaDB [(smartlabdoors)]> SELECT user_id FROM users WHERE name LIKE "Mario" AND surname LIKE "Rossi";
   -- reports the user ID, e.g. 33
   MariaDB [(smartlabdoors)]> INSERT INTO administrators(user_id, lab_id) VALUES (33, 2);
   -- with 2 being the ID of the laboratory

Note that we can nest different SQL statement, but the resulting query might become more complex to handle. Still a solution to grant administration privileges without prior knowledge on user/labs. IDs is as follows:

.. code-block:: sql

   MariaDB [(smartlabdoors)]> INSERT INTO administrators(user_id, lab_id)
        VALUES (
            (SELECT user_id FROM users WHERE surname LIKE "Rossi"),
            (SELECT lab_id FROM laboratories WHERE name LIKE "IoT lab")
            );

Adding permissions
^^^^^^^^^^^^^^^^^^

Permissions to users can be granted with commands similar to the ones used for adding administrators.

.. code-block:: sql

   MariaDB [(smartlabdoors)]> INSERT INTO permissions(user_id, lab_id, expire_date, note)
        VALUES (33, 2, "2025-03-02", "A permission given just for test");


.. note::
   The expire date should be formatted as ``%YYYY-%MM-%DD``, otherwise error are thrown by the database.
