Quickstart
==========

To start using this project, make sure you have installed everything first (see :ref:`Installation`).
If you want to create a cluster of devices, the database installation is not necessary; still for local it's recommended the database installation (see :ref:`MariaDB package installation and setup`).


Database
--------

On a newly installed MariaDB server, use the provided script to inizialize the ``smartlabdoors`` database:

.. code-block:: bash

   $ mysql -u root -p < path/to/smart-lab-doors/config/bootstrap.sql

For testing purposes, the database can be populated with some example data:

.. code-block:: bash

   $ mysql -u root -p < path/to/smart-lab-doors/config/test_populate.sql

Fell free to modify such script as you wish in order to test some specific settings.

If you want to start over with a `clean` database use the command line to delete the database

.. code-block:: bash

   MariaDB [(smartlabdoors)]> DROP DATABASE smartlabdoors;

and the reconfigure it with the bootstrap script.


Configuration
-------------

Main configuration parameters are stored into an hidden file ``.env`` stored in the ``configuration`` folder on the repository root. The file is structured as follows

.. code-block::

   # Content of config/.env

   # Mariadb configuration parameters
   DB_USER       = "pi"
   DB_PASSWORD   = "raspberry"
   DB_HOST       = "localhost"
   DB_PORT       = 3306
   DB_NAME       = "smartlabdoors"
   # Telegram bot configuration parameters
   TOKEN         = "<telegram bot token>"
   GITLAB_TOKEN  = "<gitlab token>"


The latter two parameters are used to configure the telegram bot, and are keep hidden for obvious privacy concerns.

.. note::
   In this case the configuration for the database connection uses a user ``pi`` with password ``raspberry`` on the local machine. Make sure that such user exists and has read/write access to the database:

   .. code-block::

      MariaDB [(none)]> GRANT ALL PRIVILEGES
            ON smartlabdoors.* TO "pi"@"%"
            IDENTIFIED BY "raspberry";


Running the scripts
-------------------

With everything set up, 2 python scripts in the repository's root folder can be executed:

- ``main_telebot.py`` starts the endless loop of the telegram bot service, i.e. as long as the script is running, the telegram bot is available;
- ``main_door.py`` calls the program that check one QR code. At the current stage only one check is performed and then the program stops.
