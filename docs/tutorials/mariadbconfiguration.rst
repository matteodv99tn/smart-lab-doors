MariaDB package installation and setup
======================================

This page provides some guidelines on how to install and properly setup a MariaDB server, with particular focus on critical aspects for this project.
Advices provided here are mainly related to linux-based distribution, since are the ones that I have been able to test on. Aside for the installation procedure, the command line interface should be the same in Windows based systems.


Installation and initial configuration
--------------------------------------

Installing a MariaDB server on a Debian-based distribution (such Ubuntu or the Raspian OS) it's as simple as calling

.. code-block:: shell

   $ sudo apt update
   $ sudo apt install mariadb-server


After the package has been installed, it's necessary to configure the server. This operation can be accomplished by calling the script

.. code-block:: shell

   $ mysql_secure_installation


For this project we can mainly stick with default configuration with no problems. For safety reason, I would suggest to remove anonymous users (when prompted), but to allow remote root login (for convenience when the server is not the machine of the administrator).

.. note::

   If users from multiple remote devices should access the database (as for the final architecture), the ``bind-address`` variable of the file ``/etc/mysql/mariadb.conf.d/50-server.cnf`` should be modified from ``127.0.0.1`` to ``0.0.0.0``.


Starting the service
--------------------
Once the MariaDB server has been installed, we can check it's status using the ``systemctl`` utility tool:

.. code-block::

   $ sudo systemctl status mariadb


To start or stop such service, one can call respectively the commands ``sudo systemctl start mariadb``, ``sudo systemctl stop mariadb``. For convenience one can enable MariaDB to start at each reboot of the system as

.. code-block::

   $ sudo systemctl enable mariadb


Command line interface
----------------------

Accessing a database
^^^^^^^^^^^^^^^^^^^^

While logged into the server's machine (default terminal on a localhost machine or through SSH on a remote machine), one can access the MariaDB commmand-line interface (CLI) as super-user with the command

.. code-block:: shell

   $ sudo mariadb
   MariaDB [(none)]>

Logged as super-user allows to create tables and also manage user permissions.


If we instead want to connect to a remote machine (with IP ``192.168.1.1`` as example) to a database ``my_database`` logged as user ``pi``, then the command shold be

.. code-block:: shell

   $ mariadb -u "pi" -h 192.168.1.1 -P 3306 -p my_database
   MariaDB[(my_table)]>


where ``-u`` is the user flag, ``-h`` is the hostname, ``-P`` is the port (3306 by default), ``-p`` tells to authenticate with password (that will be prompted).


Creating users
^^^^^^^^^^^^^^

As safety measure, login is required to both read and write data of a MariaDB. A super-user can add other user on the system by calling the following function

.. code-block:: shell

   MariaDB [(none)]>CREATE USER "pi"@"192.168.1.2" IDENTIFIED BY "raspberry";

In this case a process from a machine at IP ``192.168.1.2`` can access the database if it logs with username `pi` and password `raspberry`. It must be noticed that wildcards are allowed, i.e. the following command will allow access from each username `pi` (with correct password) from any location:

.. code-block:: shell

   MariaDB [(none)]>CREATE USER "pi"@"%" IDENTIFIED BY "raspberry";

More details can be found on the `official documentation <https://mariadb.com/kb/en/create-user/>`_.


Granting privileges
^^^^^^^^^^^^^^^^^^^

Being registered to the MariaDB service is not enough to read/write data. Observed that a MariaDB process can handle multiple databases, to grant all privileges to all tables for the database ``my_database`` you can use the command

.. code-block:: shell

   MariaDB [(none)]>GRANT ALL PRIVILEGES ON my_database.* TO "pi"@"192.168.1.2";


.. note::

   This command can be used also to allow admin access from any location. By calling the commands

   .. code-block:: shell

      MariaDB [(none)]>CREATE USER "root"@"%" IDENTIFIED BY "root_password";
      MariaDB [(none)]>GRANT ALL PRIVILEGES ON *.* TO "pi"@"%";


.. note::

   It is also possible to create a new user and granting him permissions by combining the syntax from the 2 commands as follows:

   .. code-block:: shell

      MariaDB [(none)]>GRANT ALL PRIVILEGES ON my_database.* TO "pi"@"localhost" IDENTIED BY "password";


Database creation
^^^^^^^^^^^^^^^^^

Once logged as root user, a new database ``new_database`` can be created simply with the following command:

.. code-block:: shell

   MariaDB [(none)]>CREATE DATABASE "new_database"

