Installation
============

Here it is explained how to install all packages needed to run the code. Installation presented are for a Debian-based distribution, such Ubuntu and the Raspiab OS.


Cloning the repository
----------------------

If you intend to update the repository, you should clone the repository using the SSH protocol as follows:

.. code-block:: bash

   git clone git@git.iotn.it:matteodv99/smart-lab-doors.git

If instead the code is only supposed to run (or at worst needs to pull update), the HTTPS protocol can also be used

.. code-block:: bash

   git clone http://git.iotn.it/matteodv99/smart-lab-doors.git


APT packages
------------

First install dependencies from the APT repositories:

.. code-block:: bash

   sudo apt update
   sudo apt upgrade
   sudo apt install -y \
        python3 python3-pip python-is-python3 gcc \
        openssl libmariadb3 libmariadb-dev


.. note::
   Depending on the state of the machine, some other packages might be needed.
   If you encounter any error, please report it to the repository maintainer so that documentation can be updated.


Python packages
---------------

Python packages can be installed using pip. The ``requirements.txt`` file in the repository root's contains all dependencies that has been tested. To install them

.. code-block:: bash

   pip3 install -r /path/to/smart-lab-doors/requirements.txt


.. note::
   Depending on the machine, some python packages might require additional tool.
   For instance while building the ``crypthography`` package on a Raspberry, the Rust compiler might be required; it can be installed with

   .. code-block:: bash

      curl https://sh.rustup.rs -sSf | sh

   At the end of the process either restart the shell or run ``source ~/.bashrc``.


OpenCV
------

For detecting the QR code, the `OpenCV <https://opencv.org/>`_ python library is used, thus needs to be installed. For most distributions this can be achieved using pip:

.. code-block:: bash

   pip install opencv-python opencv-contrib-python


However for some devices precompiled binaries are not available (or simply the python installation fails), such as for Raspberries.
The following section tells a tested method with which the package is built from source.


Build from source
^^^^^^^^^^^^^^^^^

First install the dependencies:

.. code-block:: bash

   sudo apt update && sudo apt upgrade
   sudo apt install -y \
   	   cmake build-essential pkg-config git \
       libjpeg-dev libtiff-dev libjasper-dev libpng-dev libwebp-dev libopenexr-dev \
       libavcodec-dev libavformat-dev libswscale-dev libv4l-dev libxvidcore-dev libx264-dev libdc1394-22-dev libgstreamer-plugins-base1.0-dev libgstreamer1.0-dev \
       libgtk-3-dev libqt5gui5 libqt5webkit5 libqt5test5 python3-pyqt5 \
       libatlas-base-dev liblapacke-dev gfortran \
       libhdf5-dev libhdf5-103 \
       python3-dev python3-pip python3-numpy


The next step is to clone the official github opencv repositories. In this case we clone repositories in the home directory of the user. In this case we manually chose version ``4.7.0`` since is tested to build correctly on raspberry (version ``4.8.0`` instead failed):

.. code-block:: bash

   cd ~
   git clone https://github.com/opencv/opencv.git --branch 4.7.0
   git clone https://github.com/opencv/opencv_contrib.git --branch 4.7.0


Once the repositories are cloned, we can use cmake to configure the project:

.. code-block:: bash

   mkdir -p ~/opencv/build
   cd ~/opencv/build
   cmake -D CMAKE_BUILD_TYPE=RELEASE \
       -D CMAKE_INSTALL_PREFIX=/usr/local \
       -D OPENCV_EXTRA_MODULES_PATH=~/opencv_contrib/modules \
       -D ENABLE_NEON=ON \
       -D ENABLE_VFPV3=ON \
       -D BUILD_TESTS=OFF \
       -D INSTALL_PYTHON_EXAMPLES=OFF \
       -D CMAKE_XCC_FLAGS=-latomic \
       -D OPENCV_PYTHON_INSTALL_PATH=lib/python3.9/dist-packages \
       -D BUILD_EXAMPLES=OFF ..

Finally we can build all the source code using make and exploiting all available CPU cores:

.. code-block:: bash

   cd ~/opencv/build
   make -j4

.. note::
   This operation is time consuming, might required up to few hours.


Finally we can install the targets and update the dynamic libraries

.. code-block:: bash

   cd ~/opencv/build
   sudo make install
   sudo ldconfig


Optionally it's possible to remove the source repositories that are no longer required:

.. code-block:: bash

   rm -rf ~/opencv
   rm -rf ~/opencv_contrib
