USE smartlabdoors;

-- Generating some users
INSERT INTO users(name, surname, telegram, is_student)
    VALUES ("Matteo", "Dalle Vedove", "matteodv99", TRUE),  -- should default to ID 2
           ("Davide", "Brunelli", NULL, FALSE),             -- ID 3
           ("Daniele", "Fontanelli", NULL, FALSE),          -- ID 4
           ("Mario", "Rossi", NULL, TRUE),
           ("Giovanni", "Ferrari", NULL, TRUE),
           ("Paola", "Russo", NULL, TRUE),
           ("Giovanna", "Bianchi", NULL, TRUE);


-- Generating some laboratories
INSERT INTO laboratories(name)
VALUES ("ES lab"),          -- should default to ID 1
       ("IoT lab"),         -- ID 2
       ("Robotics lab");    -- ID 3

-- Generating some administrators
INSERT INTO administrators(user_id, lab_id)
VALUES (2, 1),
       (2, 2),
       (3, 1),
       (3, 2),
       (4, 3),
       (4, 2);


-- Creating some permissions
INSERT INTO permissions(user_id, lab_id, expire_date, note)
VALUES (5, 1, "2024-01-31", "E reasonable note"),
       (6, 1, "2023-12-25", "Nothing interesting"),
       (8, 1, "2025-04-12", NULL),
       (7, 2, "2023-12-25", "It's important for a project"),
       (8, 2, "2025-04-12", NULL);


-- Creating some fake accesses
INSERT INTO accesses(permission_id)
VALUES ((SELECT permission_id FROM permissions WHERE user_id=5 AND lab_id=1)),
       ((SELECT permission_id FROM permissions WHERE user_id=6 AND lab_id=1)),
       ((SELECT permission_id FROM permissions WHERE user_id=8 AND lab_id=1)),
       ((SELECT permission_id FROM permissions WHERE user_id=7 AND lab_id=2)),
       ((SELECT permission_id FROM permissions WHERE user_id=8 AND lab_id=2));
