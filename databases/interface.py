from __future__ import annotations

from abc import ABC, abstractmethod


class DatabaseInterface(ABC):
    """ Abstract class for a database interface """

    def __init__(self, **kwargs):
        self._set_properties(**kwargs)

    def _set_properties(self, **kwargs):
        for key, value in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, value)

    @abstractmethod
    def connect(self, **kwargs) -> DatabaseInterface:
        """ Connects to the database

        Parameters
        ----------
        **kwargs
            keyword arguments that can be passed to the object for the
            connection. Depends on the final implementation.
        """
        pass

    @abstractmethod
    def disconnect(self) -> DatabaseInterface:
        """ Disconnects from the database """
        pass

    @abstractmethod
    def execute(self, query: str, params: tuple = None) -> DatabaseInterface:
        """ Executes a query

        Parameters
        ----------
        query : str
            query to be executed
        params : tuple (optional)
            parameters to be passed to the query, i.e. the values that are
            substituted to the appearance of '?' in the query string
        """
        pass

    @abstractmethod
    def fetchone(self) -> tuple:
        """ Fetches the first result of the query

        Returns
        -------
        tuple:
            results ordered as speciefied in the query
        """
        pass

    @abstractmethod
    def fetchall(self) -> list[tuple]:
        """ Fetches all the results of the query """
        pass

    @abstractmethod
    def commit(self) -> DatabaseInterface:
        """ Commits the changes to the database """
        pass
