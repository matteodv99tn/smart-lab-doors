NEW_TABLE_QUERY_USERS = """
        CREATE TABLE IF NOT EXISTS users(
            user_id         INTEGER PRIMARY KEY AUTO_INCREMENT,
            name            NVARCHAR(32) NOT NULL,
            surname         NVARCHAR(32) NOT NULL,
            email           NVARCHAR(64) UNIQUE DEFAULT NULL,
            telegram        NVARCHAR(32) UNIQUE DEFAULT NULL,
            is_student      BOOLEAN NOT NULL DEFAULT 1
            )
        """

NEW_TABLE_QUERY_LABORATORIES = """
        CREATE TABLE IF NOT EXISTS laboratories(
            lab_id          INTEGER PRIMARY KEY AUTO_INCREMENT,
            name            VARCHAR(32) NOT NULL,
            UNIQUE(name)
            )
        """

NEW_TABLE_QUERY_ADMINISTRATORS = """
        CREATE TABLE IF NOT EXISTS administrators(
            lab_id          INTEGER,
            user_id         INTEGER,
            FOREIGN KEY (user_id)   REFERENCES users(user_id),
            FOREIGN KEY (lab_id)    REFERENCES laboratories(lab_id),
            UNIQUE(lab_id, user_id)
            )
        """

NEW_TABLE_QUERY_PERMISSIONS = """
        CREATE TABLE IF NOT EXISTS permissions(
            permission_id   INTEGER PRIMARY KEY AUTO_INCREMENT,
            user_id         INTEGER,
            lab_id          INTEGER,
            admin_id        INTEGER,
            start_date      DATE    DEFAULT CURRENT_DATE,
            expire_date     DATE    NOT NULL DEFAULT '2070-01-01',
            note            TEXT    DEFAULT NULL,
            FOREIGN KEY (user_id)   REFERENCES users(user_id),
            FOREIGN KEY (lab_id)    REFERENCES laboratories(lab_id),
            FOREIGN KEY (admin_id)  REFERENCES administrators(user_id)
            )
        """

NEW_TABLE_QUERY_ACCESSES = """
        CREATE TABLE IF NOT EXISTS accesses(
            time            DATETIME NOT NULL
                            DEFAULT CURRENT_TIMESTAMP,
            permission_id   INTEGER,
            FOREIGN KEY (permission_id) REFERENCES permissions(permission_id)
            )
        """

NEW_TABLE_QUERY_TELEGRAMINFO = """
        CREATE TABLE IF NOT EXISTS telegraminfo(
            id              INTEGER,
            username        VARCHAR(32) UNIQUE,
            UNIQUE(id, username)
            )
        """


NEW_TABLE_QUERY_QRCODE_DATA = """
        CREATE TABLE IF NOT EXISTS qrcodedata(
            qr_id           INTEGER     PRIMARY KEY AUTO_INCREMENT,
            user_id         INTEGER,
            expire_date     DATETIME    NOT NULL,
            num_used        INTEGER     DEFAULT 0,
            FOREIGN KEY (user_id)       REFERENCES users(user_id)
            )
        """

NEW_TABLES_QUERIES = (NEW_TABLE_QUERY_USERS,
                      NEW_TABLE_QUERY_LABORATORIES,
                      NEW_TABLE_QUERY_ADMINISTRATORS,
                      NEW_TABLE_QUERY_PERMISSIONS,
                      NEW_TABLE_QUERY_ACCESSES,
                      NEW_TABLE_QUERY_TELEGRAMINFO,
                      NEW_TABLE_QUERY_QRCODE_DATA)


TRIGGER_DEFAULT_ADMIN = """
        CREATE TRIGGER IF NOT EXISTS add_admin_to_laboratory
            AFTER INSERT ON laboratories
        FOR EACH ROW
        BEGIN
            INSERT INTO administrators (user_id, lab_id)
            VALUES (1, NEW.lab_id);
        END;
        """

TRIGGER_ADMIN_PERMISSION = """
        CREATE TRIGGER IF NOT EXISTS give_auto_permission_admin
            AFTER INSERT ON administrators
        FOR EACH ROW
        BEGIN
            INSERT INTO permissions (user_id, lab_id, admin_id, note)
            VALUES (NEW.user_id, NEW.lab_id, 1, "Auto-admin permission");
        END;
        """

TRIGGER_QUERIES = (TRIGGER_DEFAULT_ADMIN,
                   TRIGGER_ADMIN_PERMISSION)
