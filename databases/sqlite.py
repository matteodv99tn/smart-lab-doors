from __future__ import annotations

from .interface import DatabaseInterface

import sqlite3

from functools import wraps


def ensure_connection(func):

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        if self._connection is None:
            raise RuntimeError("SqliteDatabase: Not connected to database")
        func(self, *args, **kwargs)

    return wrapper


class SqliteDatabase(DatabaseInterface):
    """ SqliteDatabase database connection

    Implementation of the :class:`DatabaseInterface` for a `Sqlite3
    <https://www.sqlite.org/index.html>`_ database using the related python
    connector.

    Parameters
    ----------
    filename : str
        The filename of the database to connect to.
        By default, the database is created in memory, this is volatile.
        Beware that if the provided file name doesn't correspond with an
        existing database, a new database will be created without warning.


    Note
    ----
    To work locally with sqlite databases, make sure to install both the basic
    sqlite3 package as well as developer tools for the connectors.

    >>> sudo apt install sqlite3 libsqlite3-dev
    >>> pip3 install pysqlite3


    """

    _filename: str = ":memory:"
    _connection: sqlite3.Connection = None
    _cursor: sqlite3.Cursor = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def connect(self, **kwargs) -> SqliteDatabase:
        self._set_properties(**kwargs)
        if self._connection is not None:
            raise RuntimeError("SqliteDatabase: Already connected to database")
        self._connection = sqlite3.connect(self._filename,
                                           check_same_thread=False)
        self._cursor = self._connection.cursor()
        return self

    @ensure_connection
    def disconnect(self) -> SqliteDatabase:
        self._connection.close()
        self._connection = None
        return self

    @ensure_connection
    def execute(self, query: str, params: tuple = None) -> SqliteDatabase:
        self._cursor.execute(query, params or ())
        return self

    @ensure_connection
    def fetchone(self) -> tuple:
        return self._cursor.fetchone()

    @ensure_connection
    def fetchall(self) -> list(tuple):
        return self._cursor.fetchall()

    @ensure_connection
    def commit(self) -> SqliteDatabase:
        self._connection.commit()
        return self

    @property
    def filename(self) -> str:
        return self._filename

    @filename.setter
    def filename(self, value: str):
        if not isinstance(value, str):
            raise TypeError("SqliteDatabse: filename must be a string")
        self._filename = value
