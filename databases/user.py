from __future__ import annotations

from . import DatabaseInterface


class User():
    """ User class

    This class contains all informations about a user as specified in the
    database architecture.

    This allows to improve code readability by avoiding to directly manage
    informations through SQL queries.


    Parameters
    ----------
    id : int
        id of the user in the database
    name : str
        name of the user
    surname : str
        surname of the user
    email : str, optional
        email address of the user
    telegram : str, optional
        telegram username of the user
    is_student : bool
        specifies whether the user is a student or not; this information is
        relevant both in user registration and access checking
    """
    id: int = -1
    name: str = None
    surname: str = None
    email: str = None
    telegram: str = None
    is_student: bool = False

    def __init__(self, **kwargs):
        self.set_properties(**kwargs)

    def set_properties(self, **kwargs):
        for key, value in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, value)

    def update_content(self, db: DatabaseInterface):
        """
        Updates the information of a user

        Given the id of a user and a valid database connection, retrieves all
        other informations of the user itself.

        Parameters
        ----------
        db : DatabaseInterface
            database connection to use for the data query

        Raises
        ------
        ValueError:
            in case the user is not found in the database
        """

        query = """
            SELECT name, surname, email, telegram, is_student
            FROM users
            WHERE user_id = ?
            """
        params = (self.id,)
        result = db.execute(query, params).fetchone()

        if result is None:
            raise ValueError(f"User with id {self.id} not found")

        self.set_properties(name=result[0],
                            surname=result[1],
                            email=result[2],
                            telegram=result[3],
                            is_student=bool(result[4]))

    def insert_into_database(self, db: DatabaseInterface):
        """
        Inserts a user into the database

        Parameters
        ----------
        db : DatabaseInterface
            database connection used to insert the user
        """
        query = """
            INSERT INTO users (name, surname, email, telegram, is_student)
            VALUES (?, ?, ?, ?, ?)
            """
        params = (self.name,  self.surname,
                  self.email, self.telegram, self.is_student)
        db.execute(query, params)
        db.commit()

        query = "SELECT LAST_INSERT_ID()"
        self.id = db.execute(query).fetchone()[0]
