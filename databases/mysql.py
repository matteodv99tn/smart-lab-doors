from __future__ import annotations

from .interface import DatabaseInterface

import sqlite3

from functools import wraps


def ensure_connection(func):

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        if self._connection is None:
            raise RuntimeError("SqliteDatabase: Not connected to database")
        func(self, *args, **kwargs)

    return wrapper


class SqliteDatabase(DatabaseInterface):

    _filename: str = ":memory:"
    _connection: sqlite3.Connection = None
    _cursor: sqlite3.Cursor = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def connect(self, **kwargs) -> SqliteDatabase:
        self._set_properties(**kwargs)
        if self._connection is not None:
            raise RuntimeError("SqliteDatabase: Already connected to database")
        self._connection = sqlite3.connect(self._filename,
                                           check_same_thread=False)
        self._cursor = self._connection.cursor()
        return self

    @ensure_connection
    def disconnect(self) -> SqliteDatabase:
        self._connection.close()
        self._connection = None
        return self

    @ensure_connection
    def execute(self, query: str, params: tuple = None) -> SqliteDatabase:
        self._cursor.execute(query, params or ())
        return self

    @ensure_connection
    def fetchone(self) -> tuple:
        return self._cursor.fetchone()

    @ensure_connection
    def fetchall(self) -> list(tuple):
        return self._cursor.fetchall()

    @ensure_connection
    def commit(self) -> SqliteDatabase:
        self._connection.commit()
        return self

    @property
    def filename(self) -> str:
        return self._filename

    @filename.setter
    def filename(self, value: str):
        if not isinstance(value, str):
            raise TypeError("SqliteDatabse: filename must be a string")
        self._filename = value
