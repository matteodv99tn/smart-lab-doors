from __future__ import annotations

from .interface import DatabaseInterface

import mariadb

from functools import wraps


def ensure_connection(func):

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        if self._connection is None:
            self.connect()

        if self._cursor is None:
            self._cursor = self._connection.cursor()

        return func(self, *args, **kwargs)

    return wrapper


class MariaDatabase(DatabaseInterface):
    """ MariadDB database connection

    Implementation of the :class:`DatabaseInterface` for `MariaDB
    <https://mariadb.org/>`_ using their python connector.


    Parameters
    ----------
    user : str
        username for the database connection
    password : str
        password for the user
    host : str
        host address of the database
    port : int
        port for the database connection
    database : str
        name of the MariaDB database to connect to


    Note
    ----
    To install the MariaDB connector for python, use the following command

    >>> pip3 install mariadb
    """

    user: str = None
    password: str = None
    host: str = None
    port: int = None
    database: str = None

    _connection: mariadb.Connection = None
    _cursor: mariadb.Cursor = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def connect(self, **kwargs) -> MariaDatabase:
        self._set_properties(**kwargs)
        if self._connection is not None:
            raise RuntimeError("MariaDatabase: Already connected to database")

        connection_args = {}
        if self.user:
            connection_args["user"] = self.user
        if self.password:
            connection_args["password"] = self.password
        if self.host:
            connection_args["host"] = self.host
        if self.port:
            connection_args["port"] = self.port
        if self.database:
            connection_args["database"] = self.database

        try:
            self._connection = mariadb.connect(**connection_args)
            self._connection.autocommit = False
        except mariadb.Error as e:
            raise RuntimeError("MariaDatabase: Could not connect to database!"
                               f"Connection error: {e}")
        self._cursor = self._connection.cursor()
        return self

    @ensure_connection
    def disconnect(self) -> MariaDatabase:
        self._connection.close()
        self._connection = None
        self._cursor = None
        return self

    @ensure_connection
    def execute(self, query: str, params: tuple | list = None) -> MariaDatabase:
        try:
            self._cursor.close()
            self._cursor = self._connection.cursor()
            if isinstance(params, list):
                self._cursor.executemany(query, params)
            else:
                self._cursor.execute(query, params or ())
        except mariadb.Error as e:
            raise RuntimeError("MariaDatabase: Error executing query!"
                               f"Query: {query}; Error: {e}")

            self.disconnect()
        return self

    @ensure_connection
    def fetchone(self) -> tuple:
        import copy
        out = self._cursor.fetchone()
        return copy.copy(out)

    @ensure_connection
    def fetchall(self) -> list(tuple):
        return self._cursor.fetchall()

    @ensure_connection
    def commit(self) -> MariaDatabase:
        self._connection.commit()
        return self
