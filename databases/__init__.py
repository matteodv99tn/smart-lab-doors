"""
Databases package
=================
This package is mainly intended to provide an abstract interface to a SQL
database (with the :class:`DatabaseInterface` class) and to provide a
implementation for both SQLite (:class:`SqliteDatabase`) and MariaDB
(:class:`MariaDatabase`).

Furthemore :class:`User` is a class designed to simplify the interaction with
the other code.
"""
from .interface import DatabaseInterface
from .sqlite import SqliteDatabase
from .mariadb import MariaDatabase

from .user import User


def get_user_by_id(db: DatabaseInterface, id: int) -> User:
    user = User(id=id)
    user.update_content(db)
    return user


def get_user_by_telegram(db: DatabaseInterface, telegram_username: str) -> User:
    query = "SELECT user_id FROM users WHERE telegram LIKE ?"
    params = (telegram_username,)
    user_id = db.execute(query, params).fetchone()

    if user_id is None:
        return None
    else:
        return get_user_by_id(db, user_id[0])


def get_user_by_fullname(db: DatabaseInterface, fullname: str) -> User:
    query = """
        SELECT user_id
        FROM users
        WHERE CONCAT(name, " ", surname) LIKE ?
        """
    params = (fullname,)
    user_id = db.execute(query, params).fetchone()

    if user_id is None:
        return None
    else:
        return get_user_by_id(db, user_id[0])


def get_lab_name_from_id(db: DatabaseInterface, lab_id: int) -> str:
    query = "SELECT name FROM laboratories WHERE lab_id = ?"
    params = (lab_id,)
    lab_name = db.execute(query, params).fetchone()

    if lab_name is None:
        return None
    else:
        return lab_name[0]


def get_administrators(db: DatabaseInterface) -> list:
    admins = []
    query = """
        SELECT user_id
        FROM users
        WHERE user_id IN (SELECT user_id FROM administrators)
            AND telegram IS NOT NULL
        """
    for (user_id, ) in db.execute(query).fetchall():
        user = User(id=user_id)
        user.update_content(db)
        admins.append(user)

    return admins
