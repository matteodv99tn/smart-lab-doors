from database import Database, User
import os.path


curr_file = os.path.abspath(__file__)
root_dir = os.path.normpath(os.path.join(curr_file, ".."))
DATABASE = os.path.join(root_dir, "test_database.db")

if os.path.exists(DATABASE):
    os.remove(DATABASE)

db = Database(DATABASE)
db.ensure_initialized()


users = [
    User(name="Admin", surname="Admin"),
    User(name="Matteo", surname="Dalle Vedove", is_student=True),
    # User(name="Matteo", surname="Dalle Vedove", telegram="matteodv99", is_student=True),
    User(name="Davide", surname="Brunelli"),
    User(name="Andrea", surname="Albanese"),
    User(name="Matteo", surname="Nardello")
    ]

laboratories = [
    "EES",
    "MSS",
    "FabLab",
    ]

[u.insert_into_database(db) for u in users]
query = "INSERT INTO laboratories (name) VALUES (?)"
[db.execute(query, (lab,)) for lab in laboratories]


query = "INSERT INTO administrators (user_id, lab_id) VALUES (?, ?)"
db.execute(query, (2, 1))
db.execute(query, (2, 2))
db.execute(query, (2, 3))
db.execute(query, (3, 1))
db.execute(query, (3, 2))
db.execute(query, (4, 1))
db.execute(query, (4, 3))
db.execute(query, (5, 3))
db.execute(query, (5, 2))

db.commit()


