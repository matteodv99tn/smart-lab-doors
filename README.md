# Build the documentation
Install python dependencies
```bash
pip3 -U sphinx sphinx_rtd_theme
```
Then build the documentation of the code; inside the root directory of the project
```bash
sphinx-apidoc -o docs/_build .
```
and then build the html
```bash
sphinx-build -b html docs ../smart-lab-doors-docs
```



# Installations and git tips
In the terminal move to the place where you want the repository to be downloaded and type
```
git clone git@git.iotn.it:matteodv99/smart-lab-doors.git IoT-project
```
In this case `IoT-project` is the name of the folder where the files will be downloaded, but you can change it.

By default you are in the `main` branch, but I suggest you to start your own branch.
First create a branch with
```
git branch alberto_devel
```
and then go into it:
```
git checkout alberto_devel
```
If you did everything correctly by typing `git status` you should see `On branch alberto_devel`.


At this point everytime you want to save the state of one or multiple files, first add them to the _staging_ area:
```
git add [file1] [file2] [...]
```
Once you added the files, you have to commit them:
```
git commit -m "Create a description of what you did here"
```

To upload changes to the server
```
git push origin
```
Maybe the first time it will complain since there's now `alberto_devel` branch on the server yet,
but follow the suggested command to fix the issue.


## Code
For now I think it's better that you organize your code in a package, call it wheter you want.
What I can provide you is a `PIL` image of the user image and you can use it to compare the photo taken by the raspberry camera.

I will also try to give you a minimal database to test out things.

# Install
To have everything running make sure to install all the following python packes
```
pip install beautifulsoup4 python-dotenv pyTelegramBotAPI sqlite3
```
Also [Sqlite3](https://www.sqlitetutorial.net/download-install-sqlite/) must be installed.

# Notes
Rasperry hostnames:
```
sld1.local
sld2.local
```
where `sld` is acronim for _Smart Lab Doors_.

