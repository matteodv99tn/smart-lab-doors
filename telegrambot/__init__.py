"""
telegrambot package
===================

This module develops all functionalities of the telegram bot.

For ease of read and maintenance, functionalities have been split into
submodules and subpackages. Each imported submodules inherits the singleton
istance of the ``bot`` object, as well as the ``db`` object w.r.t. which data
is polled and stored.
"""
import telebot
import os
from dotenv import load_dotenv

PKG_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_PRJ_DIR = os.path.normpath(os.path.join(PKG_DIR, ".."))
CONFIG_DIR = os.path.join(ROOT_PRJ_DIR, "config")

load_dotenv(CONFIG_DIR + "/.env")
BOT_TOKEN = os.getenv("TOKEN")

bot = telebot.TeleBot(BOT_TOKEN)
from .telegrambot_database import db

from . import command_handler
from . import callback_handler
from . import user
