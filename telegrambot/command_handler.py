"""
telegrambot.command_handler
===========================

This module contains the callbacks for user-triggered commands in the chat.
Some preliminary checks are performed here, but the main logic is defined in
the associated submoduled that are called.
"""
from . import bot,  db
from .utils import has_username, \
                   insert_user_chatid, \
                   user_is_admin

from .admin.adduser import adduser_start_procedure
from .admin.managepermissions import managepermissions_start_procedure
from .admin.givepermission import givepermission_start_procedure
from .user.mypermissions import mypermissions_start_procedure
from .user.newpermission import newpermission_start_procedure
from .user.registration import registration_start_procedure
from .user.qrcode import send_qrcode
from .gitlab_integration import newissue_start_procedure

from databases import get_user_by_telegram


@bot.message_handler(commands=['start'])
def start_cmd_handler(message):
    insert_user_chatid(message.from_user.username, message.chat.id)
    if not has_username(message):
        return

    user = get_user_by_telegram(db, message.from_user.username)

    if user is not None:
        text = f"Welcome back {user.name}! "\
                "You might want to use /help to see what i can do for you."
        bot.send_message(message.chat.id, text)
        return

    if message.chat.type == "group":
        text = "i'm sorry, but i can't help you in a group chat. "\
               "please, contact me directly and i'll help you."
        bot.send_message(message.chat.id, text)
        return

    registration_start_procedure(message)


@bot.message_handler(commands=['help'])
def help_cmd_handler(message):
    insert_user_chatid(message.from_user.username, message.chat.id)
    if not has_username(message):
        return

    text = "Hi, I'm a bot that will help you accessing the labs of the "\
           "University of Trento. If you are not registered yet, just "\
           "/start.\n"
    bot.send_message(message.chat.id, text)

    user = get_user_by_telegram(db, message.from_user.username)
    if user is None:
        return

    text = "If you want to enter a lab, please first create a /newpermission;"\
           " you can also see your corrently active permissions with "\
           "/mypermissions.\n"
    bot.send_message(message.chat.id, text)

    if user.is_student:
        text = "If you have an active permission for accessing a lab, you "\
               "can simply show your student eCard QR code to the reader "\
               "close to the door and you will be granted access.\n"
        bot.send_message(message.chat.id, text)
    else:
        text = "If you have an active permission for accessing a lab, you "\
               "can enter by simply showing the QR code I sent you upon "\
               "registration to the reader close to the door.\n"\
               "Still, if you lost your QR code I got you covert, just press "\
               "/qrcode"
        bot.send_message(message.chat.id, text)

    if user_is_admin(message):
        text = "Furthermore, since you are an administrator you can use more "\
               "commands:\n"\
               "/adduser: adds a new user to the system (and allows also to"\
               "give him/her permissions)\n"\
               "/managepermissions let you see all currently active "\
               "permissions and allows you to modify them\n"\
               "/givepermission let you give a new permission to an already "\
               "registered user"
        bot.send_message(message.chat.id, text)

    text = "**IMPORTANT** If there's something wrong and you want to point "\
           "it out, please use /issue to report it to the developers. "\
           "You can also suggests new features or functionalities, every "\
           "hint is welcome!"
    bot.send_message(message.chat.id, text, parse_mode="Markdown")


@bot.message_handler(commands=["newpermission"])
def newpermission_cmd_handler(message):
    insert_user_chatid(message.from_user.username, message.chat.id)
    if not has_username(message):
        return
    newpermission_start_procedure(message)


@bot.message_handler(commands=["mypermissions"])
def mypermissions_cmd_handler(message):
    insert_user_chatid(message.from_user.username, message.chat.id)
    if not has_username(message):
        return
    mypermissions_start_procedure(message)


@bot.message_handler(commands=["qrcode"])
def qrcode_cmd_handler(message):
    insert_user_chatid(message.from_user.username, message.chat.id)
    if not has_username(message):
        return

    send_qrcode(message)


@bot.message_handler(commands=["adduser"])
def adduser_cmd_handler(message):
    insert_user_chatid(message.from_user.username, message.chat.id)
    if not has_username(message):
        return

    if not user_is_admin(message):
        text = "You are not an administrator, so you can't use this command!"
        bot.send_message(message.chat.id, text)
        return

    adduser_start_procedure(message)


@bot.message_handler(commands=["managepermissions"])
def managepermissions_cmd_handler(message):
    insert_user_chatid(message.from_user.username, message.chat.id)
    if not has_username(message):
        return

    if not user_is_admin(message):
        text = "You are not an administrator, so you can't use this command!"
        bot.send_message(message.chat.id, text)
        return

    managepermissions_start_procedure(message)


@bot.message_handler(commands=["givepermission"])
def givepermission_cmd_handler(message):
    insert_user_chatid(message.from_user.username, message.chat.id)
    if not has_username(message):
        return

    if not user_is_admin(message):
        text = "You are not an administrator, so you can't use this command!"
        bot.send_message(message.chat.id, text)
        return

    givepermission_start_procedure(message)


@bot.message_handler(commands=["shutdown"])
def shutdown_cmd_handler(message):
    import os
    from time import sleep
    print("Shutting down...")
    sleep(1)
    os._exit(0)


@bot.message_handler(commands=["issue", "newissue"])
def issue_cmd_handler(message):
    insert_user_chatid(message.from_user.username, message.chat.id)
    if not has_username(message):
        return

    newissue_start_procedure(message)
