"""
telegrambot.gitlab_integration
==============================
This module develops the logic of the ``/newissue`` command by using the gitlab
python module.
"""
import gitlab
import os

from . import bot, db
from databases import get_user_by_telegram


try:
    TOKEN = os.getenv("GITLAB_TOKEN")

    gl = gitlab.Gitlab("https://git.iotn.it",
                       private_token=os.getenv("GITLAB_TOKEN"),
                       keep_base_url=True)
    gl.auth()
    project = gl.projects.get(36)
except:
    print("Error while connecting to gitlab. Check your credentials")


def newissue_start_procedure(message):
    text = "Please, give me now a short title for the issue"
    bot.send_message(message.chat.id, text)
    bot.register_next_step_handler(message, newissue_set_title)


def newissue_set_title(message):
    title = message.text
    text = "Please, insert here any information for the issue that might be "\
           "relevant"
    bot.send_message(message.chat.id, text)
    bot.register_next_step_handler(message, newissue_set_description, title)


def newissue_set_description(message, title):
    user = get_user_by_telegram(db, message.chat.username)

    if user is None:
        text = "I should have warned you before, but you are not registered, "\
               "so you can't crease issues... Please press /start"
        bot.send_message(message.chat.id, text)
        return

    desc = "### Note \n"\
           "Issue has been automatically created by the telegram bot. \n\n"\
           f"Created by: {user.name} {user.surname} "\
           f"(telegram username: {user.telegram})\n\n"\
           "### Content \n"\
           + message.text

    issue = project.issues.create({'title': title,
                                   'description': desc})
    issue.save()

    text = "Thanks for your contribution! \n"\
           "If you have access to the git project, you can follow the "\
           f"development at issue #{issue.get_id()} ({issue.web_url}).\n"
    bot.send_message(message.chat.id, text)
