"""
telegrambot.utils
=================

Contains utility functions for the telegram bot that are used in different
other modules of this package.
"""
import telebot.types
from telebot.util import quick_markup
from . import bot, db

from datetime import date
from databases import get_user_by_telegram


def insert_user_chatid(username, chat_id):
    query = "INSERT IGNORE INTO telegraminfo(id, username) VALUES (?, ?)"
    params = (chat_id, username)
    db.execute(query, params)
    db.commit()


def get_user_chatid(username):
    query = "SELECT id FROM telegraminfo WHERE username = ?"
    params = (username,)
    chatid = db.execute(query, params)\
               .fetchone()
    if chatid is None:
        return None
    else:
        return chatid[0]


def has_username(message: telebot.types.Message):
    if message.from_user.username is None:
        text = "I'm really sorry, but you must set a username for your "\
               "telegram account before being able to use me. "\
               "Please, set a username and try again, I'll wait for you!"
        bot.reply_to(message, text)
        return False
    else:
        return True


def create_entry(text: str,
                 prefix: str,
                 command: str,
                 *args) -> tuple[str, str]:
    callback_arg = prefix + ":" + command
    for arg in args:
        callback_arg += ":" + str(arg)
    return (text, callback_arg)


def generate_markup(row_width: int = 1, *args):
    return quick_markup({arg[0]: {"callback_data": arg[1]} for arg in args},
                        row_width=row_width)


def clear_markup(message):
    bot.edit_message_reply_markup(message.chat.id,
                                  message.message_id,
                                  reply_markup=None)


def user_is_admin(message):
    user = get_user_by_telegram(db, message.from_user.username)
    if user is None:
        return False

    query = """
            SELECT EXISTS(
                    SELECT user_id FROM administrators
                    WHERE user_id = ?
                    )
            """
    params = (user.id,)
    return bool(db.execute(query, params).fetchone()[0])


def string_to_date(date_str: str) -> date:
    return date(*map(int, date_str.split("-")))


def send_qrcode(message, user, caption):
    from io import BytesIO

    img = user.generate_qrcode()
    bio = BytesIO()
    bio.name = f"{user.telegram}.png"
    img.save(bio, "PNG")
    bio.seek(0)
    bot.send_photo(message.chat.id,
                   photo=bio,
                   caption=caption)
