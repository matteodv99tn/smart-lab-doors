"""
telegrambot.telegrambot_database
=================================

Instantiates the database object and connects to it.
"""
from databases import MariaDatabase
import os

user = "pi"
password = "raspberry"
host = "localhost"
port = 3306
database = "smartlabdoors"

try:
    db = MariaDatabase(user=os.getenv("DB_USER"),
                       password=os.getenv("DB_PASSWORD"),
                       host=os.getenv("DB_HOST"),
                       port=int(os.getenv("DB_PORT")),
                       database=os.getenv("DB_NAME"))
except:
    db = MariaDatabase(user=user, password=password, host=host, port=port, database=database)
