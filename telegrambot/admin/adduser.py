from .. import bot, db
from ..utils import generate_markup, \
                    create_entry, \
                    clear_markup
from databases import User, get_user_by_telegram
from datetime import date, timedelta


CBK_PRFX = "admin:adduser"

registrations = {}


@bot.callback_query_handler(func=lambda call: call.data.startswith(CBK_PRFX))
def adduser_callback_handler(call):
    args = call.data[len(CBK_PRFX)+1:].split(":")

    if args[0] == "newstudent":
        adduser_set_student(call.message)
    elif args[0] == "newguest":
        adduser_set_guest(call.message)
    elif args[0] == "addtelegram":
        adduser_ask_telegram(call.message)
    elif args[0] == "addemail":
        adduser_ask_email(call.message)
    elif args[0] == "confirm":
        adduser_add_to_database(call.message)
    elif args[0] == "cancel":
        adduser_cancel_procedure(call.message)
    elif args[0] == "nopermission":
        clear_markup(call.message)
    elif args[0] == "permissionlab":
        adduser_permission_duration_menu(call.message, args[1], args[2])
    elif args[0] == "grantpermission":
        adduser_grant_permission(call.message, args[1], args[2], args[3])


def user_description(user: User) -> str:
    if user.is_student:
        status_desc = "student"
    else:
        status_desc = "guest"

    desc = "New user (" + status_desc + ") - Collected informations\n"
    if user.name is not None:
        desc += "Name: " + user.name + "\n"
    if user.surname is not None:
        desc += "Surname: " + user.surname + "\n"
    if user.telegram is not None:
        desc += "Telegram: @" + user.telegram + "\n"
    if user.email is not None:
        desc += "Email: " + user.email + "\n"
    return desc


def adduser_start_procedure(message):
    text = "Which type of user do you want to add?"
    markup = generate_markup(2,
                             create_entry("student", CBK_PRFX, "newstudent"),
                             create_entry("guest", CBK_PRFX, "newguest"))
    bot.send_message(message.chat.id, text, reply_markup=markup)

    registrations[message.chat.id] = User()


def adduser_set_student(message):
    registrations[message.chat.id].is_student = True
    adduser_ask_name(message)


def adduser_set_guest(message):
    registrations[message.chat.id].is_student = False
    adduser_ask_name(message)


def adduser_ask_name(message):
    text = "Name of the user:"
    main_msg = bot.edit_message_text(text, message.chat.id, message.message_id)
    bot.register_next_step_handler(main_msg, adduser_ask_surname,
                                   main_msg.message_id)


def adduser_ask_surname(message, main_msg_id):
    registrations[message.chat.id].name = message.text

    text = user_description(registrations[message.chat.id])
    bot.edit_message_text(text, message.chat.id, main_msg_id)
    bot.delete_message(message.chat.id, message.message_id)

    text = "Surname of the user:"
    new_msg = bot.send_message(message.chat.id, text)
    bot.register_next_step_handler(new_msg, adduser_set_surname,
                                   main_msg_id, new_msg.message_id)


def adduser_set_surname(message, main_msg_id, other_msg_id):
    registrations[message.chat.id].surname = message.text

    text = user_description(registrations[message.chat.id])
    bot.edit_message_text(text, message.chat.id, main_msg_id)
    bot.delete_message(message.chat.id, message.message_id)
    bot.delete_message(message.chat.id, other_msg_id)
    adduser_confirmation_menu(message, main_msg_id)


def adduser_confirmation_menu(message, main_msg_id):
    text = user_description(registrations[message.chat.id])
    user = registrations[message.chat.id]
    menu_entries = list()
    if user.telegram is None:
        menu_entries.append(create_entry("Add telegram username",
                                         CBK_PRFX, "addtelegram"))
    if user.email is None:
        menu_entries.append(create_entry("Add email",
                                         CBK_PRFX, "addemail"))
    menu_entries.append(create_entry("Confirm", CBK_PRFX, "confirm"))
    menu_entries.append(create_entry("Cancel", CBK_PRFX, "cancel"))

    markup = generate_markup(1, *menu_entries)
    bot.edit_message_text(text, message.chat.id, main_msg_id,
                          reply_markup=markup)


def adduser_ask_telegram(message):
    clear_markup(message)
    text = "Telegram username of the user:"
    new_msg = bot.send_message(message.chat.id, text)
    bot.register_next_step_handler(new_msg, adduser_set_telegram,
                                   message.message_id, new_msg.message_id)


def adduser_set_telegram(message, main_msg_id, other_msg_id):
    registrations[message.chat.id].telegram = message.text

    bot.delete_message(message.chat.id, message.message_id)
    bot.delete_message(message.chat.id, other_msg_id)
    adduser_confirmation_menu(message, main_msg_id)


def adduser_ask_email(message):
    clear_markup(message)
    text = "Email of the user:"
    new_msg = bot.send_message(message.chat.id, text)
    bot.register_next_step_handler(new_msg, adduser_set_email,
                                   message.message_id, new_msg.message_id)


def adduser_set_email(message, main_msg_id, other_msg_id):
    registrations[message.chat.id].email = message.text

    bot.delete_message(message.chat.id, message.message_id)
    bot.delete_message(message.chat.id, other_msg_id)
    adduser_confirmation_menu(message, main_msg_id)


def adduser_cancel_procedure(message):
    text = message.text + "\n\n"\
           "*Operation cancelled*"
    bot.edit_message_text(text, message.chat.id, message.message_id,
                          parse_mode="Markdown")
    del registrations[message.chat.id]


def adduser_add_to_database(message):
    user = registrations.pop(message.chat.id)
    user.insert_into_database(db)

    text = message.text + "\n\n"\
        "*User added to the database*"

    bot.edit_message_text(text, message.chat.id, message.message_id,
                          parse_mode="Markdown")

    if not user.is_student:
        from utils import generate_qrcode
        from io import BytesIO
        (img, qr_id) = generate_qrcode(db, user)
        bio = BytesIO()
        bio.name = 'image.png'
        img.save(bio, 'PNG')
        bio.seek(0)
        text = f"QR code for {user.name} {user.surname}; note that this is a "\
               "never expiring QR, so please share it carefully and only if "\
               "necessary. It is recommended that each user generates each "\
               "time the QR code when needed."
        bot.send_photo(message.chat.id, bio, caption=text)

    adduser_permission_menu(message, user.id)


def adduser_permission_menu(message, user_id):

    def entry_wrapper(data: tuple):
        return create_entry("Permission for " + data[1],
                            CBK_PRFX, "permissionlab",
                            user_id, data[0])

    admin = get_user_by_telegram(db, message.chat.username)

    query = """
            SELECT lab_id, name
            FROM laboratories
            WHERE lab_id NOT IN (
                    SELECT lab_id FROM permissions
                    WHERE user_id = ?
                    )
                AND lab_id IN(
                    SELECT lab_id FROM administrators
                    WHERE user_id = ?
                    )
            """
    params = (user_id, admin.id)
    available_labs = db.execute(query, params).fetchall()
    markup = generate_markup(1, *map(entry_wrapper, available_labs),
                             create_entry("No permission for now",
                                          CBK_PRFX, "nopermission"))

    if len(available_labs) == 0:
        clear_markup(message)
        return

    bot.edit_message_reply_markup(message.chat.id, message.message_id,
                                  reply_markup=markup)


def adduser_permission_duration_menu(message, user_id, lab_id):
    def entry(desc, days):
        return create_entry(desc, CBK_PRFX, "grantpermission",
                            user_id, lab_id, days)
    markup = generate_markup(
            2,
            entry("1 week", 7),
            entry("2 weeks", 14),
            entry("1 month", 30),
            entry("3 months", 90),
            entry("6 months", 180),
            entry("1 year", 365))
    bot.edit_message_reply_markup(message.chat.id, message.message_id,
                                  reply_markup=markup)


def adduser_grant_permission(message, user_id, lab_id, amount):
    admin = get_user_by_telegram(db, message.chat.username)
    exp_date = date.today() + timedelta(days=int(amount))

    query = """
            INSERT INTO permissions (user_id, lab_id, expire_date, admin_id)
            VALUES (?, ?, ?, ?)
            """
    params = (user_id, lab_id, exp_date, admin.id)
    db.execute(query, params)
    db.commit()

    query = "SELECT name FROM laboratories WHERE lab_id = ?"
    params = (lab_id,)
    lab_name = db.execute(query, params).fetchone()[0]
    text = f"Permission granted for the lab. *{lab_name}*; "\
           f"expires on {exp_date}"
    bot.reply_to(message, text, parse_mode="Markdown")
    adduser_permission_menu(message, user_id)
