"""
telegrambot.admin.givepermission
================================

This modules implements the functionalities for granting permissions to users
"""
from .. import bot, db
from ..utils import generate_markup, \
                    create_entry, \
                    clear_markup
from databases import \
        get_user_by_telegram, \
        get_lab_name_from_id, \
        get_user_by_id

from datetime import date, timedelta


CBK_PRFX = "admin:givepermission"


@bot.callback_query_handler(func=lambda call: call.data.startswith(CBK_PRFX))
def givepermission_callback_handler(call):
    args = call.data[len(CBK_PRFX)+1:].split(":")

    if args[0] == "showlab":
        bot.delete_message(call.message.chat.id, call.message.message_id)
        givepermission_laboratory_menu(call.message, int(args[1]))
    elif args[0] == "grantperm":
        givepermission_grant(call.message, int(args[1]), int(args[2]))
    elif args[0] == "permission":
        givepermission_register(call.message, *map(int, args[1:]))
    elif args[0] == "nonote":
        bot.clear_step_handler_by_chat_id(call.message.chat.id)
        bot.delete_message(call.message.chat.id, call.message.message_id)


def givepermission_start_procedure(message):
    def entry_wrap(data):
        return create_entry(data[0], CBK_PRFX, "showlab",
                            data[1], data[0])

    query = """
            SELECT l.name, l.lab_id
            FROM administrators AS a
                INNER JOIN laboratories AS l ON l.lab_id = a.lab_id
                INNER JOIN users AS u ON u.user_id = a.user_id
            WHERE u.telegram LIKE ?
            """
    params = (message.from_user.username,)
    labs = db.execute(query, params)\
             .fetchall()

    if len(labs) == 1:
        givepermission_laboratory_menu(message, labs[0][1])
        return

    markup = generate_markup(1, *map(entry_wrap, labs))
    text = "Please, select the laboratory you want to manage permissions for:"
    bot.send_message(message.chat.id, text, reply_markup=markup)


def givepermission_laboratory_menu(message, lab_id):
    def entry_wrap(data):
        return create_entry(data[0] + " " + data[1], CBK_PRFX, "grantperm",
                            lab_id, data[2])

    lab_name = get_lab_name_from_id(db, lab_id)
    query = """
            SELECT name, surname, user_id
            FROM users
            WHERE user_id NOT IN (
                    SELECT user_id
                    FROM permissions
                    WHERE lab_id = ? AND expire_date > CURRENT_DATE
                    )
            ORDER BY name, surname
            """
    params = (lab_id,)
    user_data = db.execute(query, params).fetchall()

    markup = generate_markup(1, *map(entry_wrap, user_data))
    text = "List of registered user without permission for the lab. "\
           f"*{lab_name}*."
    bot.send_message(message.chat.id, text,
                     reply_markup=markup, parse_mode="Markdown")


def givepermission_grant(message, lab_id, user_id):
    def entry_wrap(data):
        return create_entry(data[0], CBK_PRFX, "permission",
                            lab_id, user_id, data[1])

    user = get_user_by_id(db, user_id)
    lab_name = get_lab_name_from_id(db, lab_id)

    times = [("1 day", 1),
             ("1 week", 7),
             ("1 month", 30),
             ("3 months", 90),
             ("6 months", 180),
             ("1 year", 365),
             ("Cancel", -1)]
    markup = generate_markup(2, *map(entry_wrap, times))
    text = f"Permission for *{user.name} {user.surname}* to access the lab "\
           f"*{lab_name}*"
    bot.edit_message_text(text, message.chat.id, message.message_id,
                          reply_markup=markup, parse_mode="Markdown")


def givepermission_register(message, lab_id, user_id, duration):

    if duration == -1:
        clear_markup(message)
        bot.reply_to(message, "Operation cancelled.")
        return

    admin = get_user_by_telegram(db, message.chat.username)
    exp_date = date.today() + timedelta(days=duration)

    query = """
            INSERT INTO permissions (user_id, lab_id, admin_id, expire_date)
            VALUES (?, ?, ?, ?)
            """
    params = (user_id, lab_id, admin.id, exp_date)
    db.execute(query, params)
    db.commit()

    query = "SELECT MAX(permission_id) FROM permissions"
    perm_id = db.execute(query).fetchone()[0]

    text = message.text + " granted for " + str(duration) + " days. "\
            f"Expires on {exp_date}."
    bot.edit_message_text(text, message.chat.id, message.message_id)

    text = "Leave a note now:"
    markup = generate_markup(1,
                             create_entry("No note", CBK_PRFX, "nonote"))
    msg = bot.send_message(message.chat.id, text, reply_markup=markup)
    bot.register_next_step_handler_by_chat_id(msg.chat.id,
                                              givepermission_registernote,
                                              perm_id, msg.message_id)


def givepermission_registernote(message, perm_id, bot_msg_id):
    query = """
            UPDATE permissions
            SET note = ?
            WHERE permission_id = ?
            """
    params = (message.text, perm_id)
    db.execute(query, params)
    db.commit()

    bot.delete_message(message.chat.id, bot_msg_id)
    bot.reply_to(message, "Note registered.")
