"""
telegrambot.admin.managepermissions
===================================

This modules deals with the permissions, providing the administrators a
user-friendly interface to manage them.
"""
from .. import bot, db
from ..utils import generate_markup, \
                    create_entry, \
                    clear_markup
from utils import string_to_date
from databases import get_user_by_id, get_lab_name_from_id

from datetime import timedelta


CBK_PRFX = "admin:managepermissions"

registrations = {}


@bot.callback_query_handler(func=lambda call: call.data.startswith(CBK_PRFX))
def managepermissions_callback_handler(call):
    args = call.data[len(CBK_PRFX)+1:].split(":")

    if args[0] == "labid":
        bot.delete_message(call.message.chat.id, call.message.message_id)
        managperms_active_permissions_menu(call.message, int(args[1]))
    elif args[0] == "permid":
        managperms_show_details(call.message, int(args[1]))
    elif args[0] == "terminate":
        managperms_terminate_permission(call.message, int(args[1]))
    elif args[0] == "extend":
        managperms_extend_permission(call.message, int(args[1]))
    elif args[0] == "extendby":
        mangperms_extend_permission_by_amount(call.message,
                                              int(args[1]), int(args[2]))
    elif args[0] == "changenote":
        managperms_changenote_permission(call.message, int(args[1]))
    elif args[0] == "cancel":
        clear_markup(call.message)


def managepermissions_start_procedure(message):

    def entry_wrap(data):
        return create_entry(data[0], CBK_PRFX, "labid", data[1])

    query = """

            SELECT l.name, l.lab_id
            FROM administrators AS a
                INNER JOIN laboratories AS l ON l.lab_id = a.lab_id
                INNER JOIN users AS u ON u.user_id = a.user_id
            WHERE u.telegram LIKE ?
            """
    params = (message.from_user.username,)
    labs = db.execute(query, params)\
             .fetchall()

    if len(labs) == 0:
        managperms_active_permissions_menu(message, labs[0][1])
        return

    text = "Please, select the laboratory you want to manage permissions for:"
    markup = generate_markup(1, *[entry_wrap(lab) for lab in labs])
    bot.send_message(message.chat.id, text, reply_markup=markup)


def managperms_active_permissions_menu(message, lab_id: int):

    def entry_wrap(data):
        return create_entry(data[0], CBK_PRFX, "permid", data[1])

    query = """
            SELECT CONCAT(u.name, " ", u.surname), p.permission_id
            FROM permissions AS p
                INNER JOIN users AS u ON u.user_id = p.user_id
                INNER JOIN laboratories AS l ON l.lab_id = p.lab_id
            WHERE l.lab_id = ?
                AND u.user_id NOT IN (
                        SELECT user_id FROM administrators
                        WHERE lab_id = ?
                        )
                AND p.expire_date > CURRENT_DATE
            ORDER BY p.expire_date ASC
            """
    params = (lab_id, lab_id)
    perms = db.execute(query, params)\
              .fetchall()
    labname = get_lab_name_from_id(db, lab_id)

    if len(perms) == 0:
        text = f"There are no active permissions for the lab. {labname}."
        bot.send_message(message.chat.id, text, parse_mode="Markdown")
        return

    text = f"Currently active permissions for the lab. *{labname}*."
    markup = generate_markup(1, *[entry_wrap(p) for p in perms])
    bot.send_message(message.chat.id, text, reply_markup=markup,
                     parse_mode="Markdown")


def managperms_show_details(message, perm_id):

    def entry_wrap(desc, code):
        return create_entry(desc, CBK_PRFX, code, perm_id)

    query = """
            SELECT lab_id, admin_id, user_id, note, start_date, expire_date
            FROM permissions
            WHERE permission_id = ?
            """
    params = (perm_id,)
    data = db.execute(query, params)\
             .fetchone()
    lab_id = data[0]
    admin_id = data[1]
    user_id = data[2]
    note = data[3]
    # start_date = string_to_date(data[4])
    # expire_date = string_to_date(data[5])
    start_date = data[4]
    expire_date = data[5]
    remaining_days = (expire_date - start_date).days

    admin = get_user_by_id(db, admin_id)
    user = get_user_by_id(db, user_id)
    lab_name = get_lab_name_from_id(db, lab_id)

    text = f"Permission #{perm_id} granted to *{user.name} {user.surname}* "\
           f"for the lab *{lab_name}* by {admin.name} {admin.surname}\n"\
           f"Granted on {start_date}, expires on {expire_date} "\
           f"({remaining_days} days left)."

    if note is not None:
        text += f"\n\nNote: {note}"

    markup = generate_markup(1,
                             entry_wrap("Terminate permission", "terminate"),
                             entry_wrap("Extend permission", "extend"),
                             entry_wrap("Change note", "changenote"),
                             entry_wrap("Cancel", "cancel"))
    bot.edit_message_text(text, message.chat.id, message.message_id,
                          reply_markup=markup, parse_mode="Markdown")


def managperms_terminate_permission(message, perm_id):
    query = """
            UPDATE permissions
            SET expire_date = CURRENT_DATE
            WHERE permission_id = ?
            """
    params = (perm_id,)
    db.execute(query, params)

    query = "SELECT user_id FROM permissions WHERE permission_id = ?"
    user_id = db.execute(query, params).fetchone()[0]

    clear_markup(message)
    text = "Permission has been terminated."
    bot.reply_to(message, text)

    user = get_user_by_id(db, user_id)
    # TODO: notify user


def managperms_extend_permission(message, perm_id):

    def entry_wrap(desc, amount):
        return create_entry(desc, CBK_PRFX, "extendby", perm_id, amount)

    markup = generate_markup(2,
                             entry_wrap("1 week", 7),
                             entry_wrap("2 weeks", 14),
                             entry_wrap("1 month", 30),
                             entry_wrap("3 months", 90),
                             entry_wrap("6 months", 180),
                             entry_wrap("1 year", 365))
    bot.edit_message_reply_markup(message.chat.id, message.message_id,
                                  reply_markup=markup)


def mangperms_extend_permission_by_amount(message, perm_id, amount):
    query = "SELECT expire_date FROM permissions WHERE permission_id = ?"
    params = (perm_id,)
    exp_date = string_to_date(db.execute(query, params).fetchone()[0])

    new_exp_date = exp_date + timedelta(days=amount)
    query = """
            UPDATE permissions
            SET expire_date = ?
            WHERE permission_id = ?
            """
    params = (new_exp_date, perm_id)
    db.execute(query, params)
    db.commit()
    managperms_show_details(message, perm_id)


def managperms_changenote_permission(message, perm_id):
    clear_markup(message)
    text = "Please, enter the new note for the permission:"
    new_msg = bot.send_message(message.chat.id, text)

    bot.register_next_step_handler_by_chat_id(message.chat.id,
                                              managperms_setnewnote,
                                              perm_id, message, new_msg)


def managperms_setnewnote(message, perm_id, original_msg, other_msg):
    bot.delete_message(message.chat.id, message.message_id)
    bot.delete_message(message.chat.id, other_msg.message_id)

    query = "UPDATE permissions SET note = ? WHERE permission_id = ?"
    params = (message.text, perm_id)
    db.execute(query, params)
    db.commit()

    managperms_show_details(original_msg, perm_id)
