"""
telegrambot.user.newpermission
==============================
This module contains the handlers for the ``/newpermission`` command.
"""
from .. import bot, db
from ..utils import generate_markup, \
                    create_entry, \
                    clear_markup, \
                    get_user_chatid

from databases import get_user_by_telegram
from datetime import date, timedelta


CBK_PRFX = "newpermission"


@bot.callback_query_handler(func=lambda call: call.data.startswith(CBK_PRFX))
def registration_callback_handler(call):
    args = call.data[len(CBK_PRFX)+1:].split(":")

    if args[0] == "labid":
        newpermission_admin_menu(call.message, args[1])
    elif args[0] == "requesttoadmin":
        newpermission_forward_request_to_admin(call.message, args[1], args[2])
    elif args[0] == "accept":
        newpermission_request_accepted(call.message, args[1], args[2], args[3])
    elif args[0] == "deny":
        newpermission_request_denied(call.message, args[1], args[2])
    elif args[0] == "nomemo":
        newpermission_no_note(call.message, args[1])


def newpermission_start_procedure(message):

    def entry_wrapper(data: tuple):
        return create_entry(data[0],
                            CBK_PRFX, "labid",
                            data[1])

    user = get_user_by_telegram(db, message.from_user.username)
    if user is None:
        text = "Seems that you are not registered... Maybe call /start first?"
        bot.send_message(message.chat.id, text)
        return

    query = """
            SELECT name, lab_id
            FROM laboratories
            WHERE lab_id NOT IN (
                SELECT lab_id FROM permissions
                WHERE user_id = ?
                    AND expire_date > CURRENT_TIMESTAMP
            )
            """
    params = (user.id,)
    res = db.execute(query, params).fetchall()

    if len(res) == 0:
        text = "It seems that you already have permissions for all available "\
                "labs, so I can't help you any further...\n"\
                "Maybe take a look at /mypermissions"
        bot.send_message(message.chat.id, text)
        return

    text = "That's what's available for you:"
    markup = generate_markup(1, *map(entry_wrapper, res))
    bot.send_message(message.chat.id, text, reply_markup=markup)


def newpermission_admin_menu(message, lab_id):

    def entry_wrapper(data: tuple):
        return create_entry(data[0] + " " + data[1],
                            CBK_PRFX, "requesttoadmin",
                            lab_id, data[2])

    query = "SELECT name FROM laboratories WHERE lab_id = ?"
    params = (lab_id,)
    labname = db.execute(query, params).fetchone()[0]
    text = f"Ok, let's create a new permission for the lab \"{labname}\"."
    bot.edit_message_text(text, message.chat.id, message.message_id)

    query = """
            SELECT name, surname, telegram
            FROM users
            WHERE user_id IN (
                SELECT user_id FROM administrators
                WHERE lab_id = ?
                )
            AND telegram IS NOT NULL
            """
    params = (lab_id,)
    res = db.execute(query, params).fetchall()

    if len(res) == 1:
        print(res[0][2])
        newpermission_forward_request_to_admin(message, lab_id,
                                               res[0][2], False)
        return

    text = "Which administrators do you want to ask?"
    markup = generate_markup(1, *map(entry_wrapper, res))
    bot.send_message(message.chat.id, text, reply_markup=markup)


def newpermission_forward_request_to_admin(message, lab_id, admin_username,
                                           has_markup = True):
    if has_markup:
        clear_markup(message)
    text = "Ok, request forwarded to the administrator... "\
           "I'll let you know as soon as possible!"
    bot.send_message(message.chat.id, text)

    query = "SELECT username FROM telegraminfo WHERE id = ?"
    params = (message.chat.id,)
    user_username = db.execute(query, params).fetchone()[0]
    user = get_user_by_telegram(db, user_username)
    admin_chatid = get_user_chatid(admin_username)
    query = "SELECT name FROM laboratories WHERE lab_id = ?"
    params = (lab_id,)
    labname = db.execute(query, params).fetchone()[0]
    text = f"New permission request for the lab *{labname}*. "\
           "User's informations:\n"\
           f"Name: {user.name}\n"\
           f"Surname: {user.surname}\n"\
           f"Telegram: @{user.telegram}\n"\
           f"Email: {user.email}\n"
    markup = generate_markup(
            2,
            create_entry("1 week", CBK_PRFX, "accept",
                         lab_id, user.telegram, 7),
            create_entry("2 weeks", CBK_PRFX, "accept",
                         lab_id, user.telegram, 14),
            create_entry("1 month", CBK_PRFX, "accept",
                         lab_id, user.telegram, 30),
            create_entry("3 months", CBK_PRFX, "accept",
                         lab_id, user.telegram, 90),
            create_entry("6 months", CBK_PRFX, "accept",
                         lab_id, user.telegram, 180),
            create_entry("1 year", CBK_PRFX, "accept",
                         lab_id, user.telegram, 365),
            create_entry("Deny", CBK_PRFX, "deny", lab_id, user.telegram),
            )
    bot.send_message(admin_chatid, text, reply_markup=markup,
                     parse_mode="Markdown")


def newpermission_request_denied(message, lab_id, user_telegram):
    text = message.text + "\n\n"\
           "Permission denied"
    bot.edit_message_text(text, message.chat.id, message.message_id)

    query = "SELECT name FROM laboratories WHERE lab_id = ?"
    params = (lab_id,)
    labname = db.execute(query, params).fetchone()[0]
    text = f"Unfortunately your request for the lab {labname} has been "\
           "denied by the administrator... Maybe reach him privately next time"
    user_chatid = get_user_chatid(user_telegram)
    bot.send_message(user_chatid, text)


def newpermission_request_accepted(message, lab_id, user_telegram, days):
    expire_date = date.today() + timedelta(days=int(days))
    admin = get_user_by_telegram(db, message.chat.username)
    user = get_user_by_telegram(db, user_telegram)
    query = """
            INSERT INTO permissions (user_id, lab_id, admin_id, expire_date)
            VALUES (?, ?, ?, ?)
            """
    params = (user.id, lab_id, admin.id, expire_date)
    db.execute(query, params)

    query = "SELECT MAX(permission_id) FROM permissions"
    permission_id = db.execute(query).fetchone()[0]

    permission_text = message.text + "\n\n"\
        f"Accepted, expires on {expire_date}"
    text = message.text + "\n\n"\
        "If you want, leave a note as memo for the permission"
    markup = generate_markup(
            1,
            create_entry("No memo", CBK_PRFX, "nomemo", permission_id))
    msg = bot.edit_message_text(text, message.chat.id, message.message_id,
                                reply_markup=markup)
    bot.register_next_step_handler(message, newpermission_add_note,
                                   permission_text, permission_id, msg.id)

    query = "SELECT name FROM laboratories WHERE lab_id = ?"
    params = (lab_id,)
    labname = db.execute(query, params).fetchone()[0]
    text = f"Your request for the lab {labname} has been accepted "\
           "congratulations! \n"\
           f"Your permission will expire on {expire_date}"
    user_chatid = get_user_chatid(user_telegram)
    bot.send_message(user_chatid, text)


def newpermission_add_note(message, permission_text,
                           permission_id, message_id):
    text = permission_text + "\n"\
        f"Note: {message.text}"
    bot.edit_message_text(text, message.chat.id, message_id)
    bot.delete_message(message.chat.id, message.message_id)

    query = "UPDATE permissions SET note = ? WHERE permission_id = ?"
    params = (message.text, permission_id)
    db.execute(query, params)
    db.commit()


def newpermission_no_note(message, permission_id):
    text = message.text[:message.text.rfind("\n\n")]
    bot.edit_message_text(text, message.chat.id, message.message_id)
    bot.clear_step_handler_by_chat_id(message.chat.id)
