"""
telegrambot.user.registration
=============================

This module contains the procedure for registering a new user into the
platform.
"""
from .. import bot, db
from ..utils import generate_markup, \
                    create_entry, \
                    clear_markup, \
                    get_user_chatid
from databases import User, get_user_by_fullname, get_administrators


CBK_PRFX = "registration"

registrations = {}


@bot.callback_query_handler(func=lambda call: call.data.startswith(CBK_PRFX))
def registration_callback_handler(call):
    args = call.data[len(CBK_PRFX)+1:].split(":")

    if args[0] == "student":
        registration_register_student(call.message)
    elif args[0] == "guest":
        registration_register_guest(call.message)
    elif args[0] == "confirmdata":
        registration_adduser_continue(call.message)
    elif args[0] == "denydata":
        registration_cancel_procedure(call.message)
    elif args[0] == "askadmin":
        registration_ask_admin(call.message, args[1])
    elif args[0] == "adminaccept":
        registration_admin_accepted(call.message, int(args[1]))
    elif args[0] == "admindeny":
        registration_admin_denied(call.message, args[1])
    elif args[0] == "duplicate":
        registration_check_user_already_registered(call.message, int(args[1]))
    elif args[0] == "nodup":
        registration_check_user_not_registered(call.message)


def registration_start_procedure(message):
    msg_1 = "Welcome! It seems that you are not a registered user for "\
            "entering the laboratories of the University of Trento, but "\
            "dont't worry, we can fix this now..."
    msg_2 = "(Quick note: since I'm a little dumb, please follow my "\
            "instructions carefully... if something goes crazy, feel free to "\
            "restart the procedure by typing /start again)"
    bot.send_message(message.chat.id, msg_1)
    bot.send_message(message.chat.id, msg_2)
    registration_ask_student(message)


def registration_ask_student(message):
    text = "Before starting, are you a student of the university?"
    markup = generate_markup(
            2,
            create_entry("Yep!", CBK_PRFX, "student"),
            create_entry("Nope!", CBK_PRFX, "guest")
            )
    bot.send_message(message.chat.id, text, reply_markup=markup)

    registrations[message.chat.id] = User()
    registrations[message.chat.id].telegram = message.from_user.username


def registration_register_student(message):
    registrations[message.chat.id].is_student = True
    edit_msg = "Dear student, thank for joining us!"\
               "Please, read this note carefully:"
    new_msg = "For the registration I will ask your name and surname. "\
              "Please, be sure that informations are matching the ones "\
              "reported in Esse3, otherwise accessing the labs through the "\
              "student eCard will fail!\n"\
              "At least I warned you..."
    bot.edit_message_text(edit_msg, message.chat.id, message.message_id)
    bot.send_message(message.chat.id, new_msg)
    registration_ask_name(message)


def registration_register_guest(message):
    registrations[message.chat.id].is_student = False
    edit_msg = "Dear guest, thank for joining us!"\
               "Let's continue with the registration procedure... At the end "\
               "I will give you a QR code that you will use to enter the labs."
    bot.edit_message_text(edit_msg, message.chat.id, message.message_id)
    registration_ask_name(message)


def registration_ask_name(message):
    text = "Let's start simple... What's your name?"
    bot.send_message(message.chat.id, text)
    bot.register_next_step_handler_by_chat_id(message.chat.id,
                                              registration_ask_surname)


def registration_ask_surname(message):
    name = message.text
    registrations[message.chat.id].name = name
    text = f"Cool, {name}! Now, what's your surname?"
    bot.send_message(message.chat.id, text)
    bot.register_next_step_handler_by_chat_id(message.chat.id,
                                              registration_set_surname)


def registration_set_surname(message):
    registrations[message.chat.id].surname = message.text
    curr_user = registrations[message.chat.id]
    full_name = f"{curr_user.name} {curr_user.surname}"
    user = get_user_by_fullname(db, full_name)

    if user is None:
        registration_confirmation_menu(message)
    else:
        registration_check_user_duplicate(message, user)


def registration_check_user_duplicate(message, user):
    text = "Hold on, hold on... By looking in my database I already found "\
           "some with the same name and surname... that's strange... \n"\
           "Could it be that an administrator already registered you?"
    markup = generate_markup(
            1,
            create_entry("Yes, I think so...", CBK_PRFX, "duplicate", user.id),
            create_entry("No, I'm sure I'm not registered!", CBK_PRFX, "nodup")
            )
    bot.send_message(message.chat.id, text, reply_markup=markup)


def registration_check_user_already_registered(message, userid):
    clear_markup(message)
    text = "Yeah, I knew it! \n"\
           "I'll bind your telegram account to the user in the database and "\
           "that's it. Now you can start asking permission with "\
           "/newpermission command; or simply check your permissions with "\
           "/mypermissions"
    bot.send_message(message.chat.id, text)
    del registrations[message.chat.id]

    query = """
            UPDATE users
            SET telegram = ?
            WHERE user_id = ?
            """
    params = (message.chat.username, userid)
    db.execute(query, params)
    db.commit()
    registration_info_message(message)


def registration_check_user_not_registered(message):
    clear_markup(message)
    text = "No issue then, we can continue anyway"
    bot.send_message(message.chat.id, text)
    registration_confirmation_menu(message)


def registration_confirmation_menu(message):
    text = "Ok, let's recap what you told me...\n"\
           f"Name: {registrations[message.chat.id].name}\n"\
           f"Surname: {registrations[message.chat.id].surname}\n\n"\
           "Do you confirm everything?"
    markup = generate_markup(
            1,
            create_entry("Everything is correct!", CBK_PRFX, "confirmdata"),
            create_entry("Something is wrong...", CBK_PRFX, "denydata")
            )
    bot.send_message(message.chat.id, text, reply_markup=markup)


def registration_cancel_procedure(message):
    clear_markup(message)
    text = "Ohh, it's unfortunate that there are wrong informations... "\
           "I suggest you to /start from the beginning, just press the "\
           "command!"
    bot.send_message(message.chat.id, text)
    registrations.pop(message.chat.id)


def registration_adduser_continue(message):
    clear_markup(message)
    if registrations[message.chat.id].is_student:
        registration_adduser_student(message)
    else:
        registration_adduser_guest(message)


def registration_adduser_student(message):
    user = registrations.pop(message.chat.id)
    user.insert_into_database(db)
    text = "Perfect! You have been added to the database... "\
           "You can now press /newpermission to grant you permission for "\
           "entering the labs!"
    bot.send_message(message.chat.id, text)
    registration_info_message(message)


def registration_adduser_guest(message):
    def entry_wrapper(user):
        return create_entry(user.name + " " + user.surname,
                            CBK_PRFX, "askadmin",
                            user.telegram)
    admins = get_administrators(db)
    if len(admins) == 0:
        print("WARNING: no admins")

    markup = generate_markup(
            1,
            *map(entry_wrapper, admins)
            )
    text = "Unfortunately you are not a student, so an administrator must "\
           "approve your request... Who do you want to contact?"
    bot.send_message(message.chat.id, text, reply_markup=markup)


def registration_ask_admin(message, admin_username: str):
    clear_markup(message)
    text = "I'm forwarding your request... I'll hit you as soon as I have "\
           "news!"
    bot.send_message(message.chat.id, text)

    admin_chat_id = get_user_chatid(admin_username)
    text = "A new guest wants to be register...\n"\
           f"Name: {registrations[message.chat.id].name}\n"\
           f"Surname: {registrations[message.chat.id].surname}"\
           f"Telegram: @{registrations[message.chat.id].telegram}"\
           f"E-mail: {registrations[message.chat.id].email}"
    markup = generate_markup(
            2,
            create_entry("Accept", CBK_PRFX, "adminaccept", message.chat.id),
            create_entry("Deny", CBK_PRFX, "admindeny", message.chat.id)
            )
    bot.send_message(admin_chat_id, text, reply_markup=markup)


def registration_admin_accepted(message, newuser_chatid: int):
    text = message.text + "\n\nRequest accepted"
    bot.edit_message_text(text, message.chat.id, message.message_id)

    text = "Congratulations! Your request has been accepted!"
    bot.send_message(newuser_chatid, text)
    user = registrations.pop(newuser_chatid)
    user.insert_into_database(db)

    text = "You can now press /newpermission to ask permissions for "\
           "entering the labs!"
    bot.send_message(newuser_chatid, text)

    text = "Once your permissions are set, when close to doors press /qrcode "\
           "and I will give you a QR code to show to the camera!\n"\
           "Beware that this QR code has a limited lifetime and can only be "\
           "used once (but of course you can ask me how many qrcodes you want"
    msg = bot.send_message(newuser_chatid, text)
    registration_info_message(msg)


def registration_admin_denied(message, newuser_chatid: int):
    text = message.text + "\n\nRequest denied"
    bot.edit_message_text(text, message.chat.id, message.message_id)

    text = "Unfortunately your request has been denied..."
    bot.send_message(newuser_chatid, text)
    registrations.pop(newuser_chatid)


def registration_info_message(message):
    text = "For further information, you can always write /help and I will "\
           "tell everything that you can do with me"
    bot.send_message(message.chat.id, text)
