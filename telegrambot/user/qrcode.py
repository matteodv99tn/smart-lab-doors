"""
telegrambot.user.qrcode
=======================

This module contains the handlers for the ``/qrcode`` command.
"""
from .. import bot, db

from utils import generate_qrcode
from databases import get_user_by_telegram

from io import BytesIO


QRCODE_DURATION_MINS = 2


def send_qrcode(message):
    user = get_user_by_telegram(db, message.from_user.username)
    (img, qr_id) = generate_qrcode(db, user, QRCODE_DURATION_MINS)
    bio = BytesIO()
    bio.name = 'image.png'
    img.save(bio, 'PNG')
    bio.seek(0)

    text = "This is the qrcode that you can use for accessing the lab..."
    msg = bot.send_photo(message.chat.id, bio, caption=text)

    from time import sleep
    sleep(QRCODE_DURATION_MINS * 60)
    delete_message(msg.chat.id, msg.message_id, message.message_id)


def delete_message(chat_id, img_message_id, command_message_id):
    bot.delete_message(chat_id, img_message_id)
    text = "The QR code validity has expired"
    bot.send_message(chat_id, text,
                     reply_to_message_id=command_message_id)
