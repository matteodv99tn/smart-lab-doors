"""
telegrambot.user.mypermissions
==============================

This module contains the functions to handle the ``/mypermissions`` command.
"""
from .. import bot, db


CBK_PRFX = "mypermissions"


# @bot.callback_query_handler(func=lambda call: call.data.startswith(CBK_PRFX))
# def registration_callback_handler(call):
#     args = call.data[len(CBK_PRFX)+1:].split(":")


def mypermissions_start_procedure(message):
    query = """
            SELECT l.name, p.expire_date
            FROM permissions AS p
                INNER JOIN laboratories AS l ON p.lab_id = l.lab_id
            WHERE p.user_id = (
                    SELECT user_id  FROM users WHERE telegram LIKE ?
                    )
                AND p.expire_date > date('now')
            ORDER BY p.expire_date ASC
            """
    params = (message.from_user.username,)
    permissions = db.execute(query, params).fetchall()

    if len(permissions) == 0:
        text = "Seems that you don't have any active permissions... Try "\
               "/newpermission to create one!"
        bot.send_message(message.chat.id, text)
        return

    text = "Here are your active permissions:\n"
    for (lab_name, exp_date) in permissions:
        text += f"> *{lab_name}*, expires on {exp_date}\n"

    bot.send_message(message.chat.id, text, parse_mode="Markdown")
