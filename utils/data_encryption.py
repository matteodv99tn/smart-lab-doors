
from cryptography.fernet import Fernet

FILE_DIR = "key"


def generate_key():
    """ Generate and saves encryption key """
    import os

    if not os.path.exists(FILE_DIR):
        os.mkdir(FILE_DIR)

    key = Fernet.generate_key()
    full_file_name = os.path.join(FILE_DIR, "qrcode.key")
    with open(full_file_name, "wb") as f:
        f.write(key)

    return key


def load_key():
    """ Loads stored keys """
    import os

    full_file_name = os.path.join(FILE_DIR, "qrcode.key")

    if os.path.exists(full_file_name):
        with open(full_file_name, 'rb') as pub:
            return pub.read()
    else:
        return generate_key()


def encrypt_data(data):
    key = load_key()
    fernet = Fernet(key)
    return fernet.encrypt(data.encode())


def decrypt_data(data):
    key = load_key()
    fernet = Fernet(key)
    return fernet.decrypt(data).decode()


if __name__ == "__main__":
    key = generate_key()

    original_data = "Hello World"
    encrypted_data = encrypt_data(original_data)
    decrypted_data = decrypt_data(encrypted_data)

    print(f"Original Data: {original_data}")
    print(f"Encrypted Data: {encrypted_data}")
    print(f"Decrypted Data: {decrypted_data}")
