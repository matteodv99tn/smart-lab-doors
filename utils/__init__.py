from .data_encryption import encrypt_data, decrypt_data
from databases import DatabaseInterface, get_user_by_id

import qrcode

# from database import Database
# from database.user import User
from datetime import datetime, timedelta, date
from PIL import Image

DEFAULT_EXPIRE_DATE = datetime(2070, 1, 1)
GUEST_QR_PREFIX = "guestuseraccess"


def string_to_date(date_str: str) -> date:
    return date(*map(int, date_str.split("-")))


def string_to_datetime(datetime_str: str) -> datetime:
    return datetime.strptime(datetime_str.split(".")[0], "%Y-%m-%d %H:%M:%S")


def generate_qrcode(db,
                    user,
                    qr_timespan: int = None):
    """
    qr_timespan is the expected validity of the qrcode in minutes.

    Returns the generated qrcode image and the corresponding qr_id.
    """
    if qr_timespan is None:
        expire_date = DEFAULT_EXPIRE_DATE
    else:
        curr_time = datetime.now()
        expire_date = curr_time + timedelta(minutes=qr_timespan)

    query = """
            INSERT INTO qrcodedata (user_id, expire_date)
            VALUES (?, ?)
            """
    params = (user.id, expire_date)
    db.execute(query, params)
    db.commit()

    query = " SELECT MAX(qr_id) FROM qrcodedata "
    qr_id = db.execute(query).fetchone()[0]

    qr_data = encode_qrcode_data(qr_id)
    qr = qrcode.QRCode(version=1,
                       error_correction=qrcode.constants.ERROR_CORRECT_L,
                       box_size=10,
                       border=4)
    qr.add_data(qr_data)
    qr.make(fit=True)
    img = qr.make_image(fill_color="black", back_color="white")

    return (img, qr_id)


def encode_qrcode_data(qr_id) -> str:
    qr_data = f"{GUEST_QR_PREFIX}:{qr_id}"
    return encrypt_data(qr_data)


def check_qr_validity(db: DatabaseInterface,
                      qr_id  #: int):
                      ):
    """
    Returns
    - a User object if the qrcode is valid
    - None if the qrcode is no more valid
    """
    query = """
            SELECT expire_date, num_used, user_id
            FROM qrcodedata
            WHERE qr_id = ?
            """
    params = (qr_id,)
    res = db.execute(query, params).fetchone()
    (expire_date, num_used, user_id) = res
    # expire_date = string_to_datetime(expire_date)
    expire_date = expire_date

    expired = expire_date < datetime.now()
    used = num_used > 0
    adminqr = expire_date == DEFAULT_EXPIRE_DATE

    if not expired and (not used or adminqr):
        query = "UPDATE qrcodedata SET num_used = ? WHERE qr_id = ?"
        params = (num_used + 1, qr_id)
        db.execute(query, params)
        db.commit()
        return get_user_by_id(db, user_id)
    else:
        return False
